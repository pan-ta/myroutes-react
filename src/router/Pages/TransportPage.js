import React, {Fragment} from 'react';
import {connect} from 'react-redux';

import PageWrapper from 'components/Wrappers/PageWrapper/PageWrapper';
import WrapperWide from 'components/Wrappers/WrapperWide/WrapperWide';
import WrapperNarrowWhite from 'components/Wrappers/WrapperNarrow/WrapperNarrowWhite';
import Header from 'components/Header/Header';
import EditSection from 'components/Sections/EditSection/EditSection';
import IntroSection from 'components/Sections/IntroSection/IntroSection';
import TableSection from 'components/Sections/TableSection/TableSection';
import SpacerWhite from 'components/Spacer/SpacerWhite.js';
import Footer from 'components/Footer/Footer.js';

import {getPagedData} from 'utils';

const TransportPage = (props) => (
    <Fragment>
        <PageWrapper>
            <Header user={props.user} dropdowns={props.dropdowns}/>
            <WrapperWide>
                <WrapperNarrowWhite>
                    <EditSection
                        user={props.user}
                        adminPanel={props.adminPanel}
                        data={props.dataTransport}
                    />
                    <IntroSection
                        user={props.user}
                        title={"Vehicles"} 
                        desc={"Here you can find a list of vehicles provided by BRUGGE BRI & LL. Find detailed information about the vehicle by clicking on the appropriate links. For quick search use the sort and filter buttons located in the header of the table."} 
                    />
                    <TableSection 
                        table="transport"
                        entity={"vehicle"}
                        data={props.dataTransport}
                        user={props.user}
                        currentSorting={props.currentSorting.transport}
                        currentPaging={props.currentPaging}
                        tableFilterBlockVisibility={props.tableFilterBlockVisibility.transport}
                        tableFilterInput={props.tableFilterInput.transport}
                        adminPanel={props.adminPanel}
                        />
                </WrapperNarrowWhite>
            </WrapperWide>
            <SpacerWhite />
            <Footer />
        </PageWrapper>
    </Fragment>
)



const mapStateToProps = store => {
    console.log(store);
    return {
        user: store.user,
        dropdowns: store.dropdowns,
        dataTransport: getPagedData(store.data.transport, store.currentPaging.transport),
        currentSorting: store.currentSorting,
        currentPaging: {
            ...store.currentPaging.transport,
            totalPages: Math.ceil(store.data.transport.length / store.currentPaging.transport.pageSize)
        },
        tableFilterBlockVisibility: store.tableFilterBlockVisibility,
        tableFilterInput: store.tableFilterInput,
        adminPanel: store.adminPanel

    };
}

export default connect(mapStateToProps)(TransportPage);
