import React from 'react';
import './AdminCloseBtn.scss';




const AdminCloseBtn = ({onClick}) => (
    <button className="close-btn close-btn_admin" onClick={onClick}>
        <i className="fas fa-times"></i>
    </button>
);


export default AdminCloseBtn;

