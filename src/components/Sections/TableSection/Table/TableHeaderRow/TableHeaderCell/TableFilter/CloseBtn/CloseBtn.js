import React from 'react';
import './CloseBtn.scss';
import {actions} from 'actions';
import {connect} from 'react-redux';



const CloseBtn = ({closeFilterBlocks, clearInputs}) => {
    const onCloseBtnClick = () => {
        closeFilterBlocks();
        clearInputs();
    }

    return(
        <button onClick={onCloseBtnClick} className="close-btn" id="close-btn">
            <i className="fas fa-times"></i>
        </button>
    )
};

// export {CloseBtn};

const mapDispatchToProps = dispatch => {
    return {
        closeFilterBlocks: () => dispatch(actions.TableFilterBlockVisibilityActions.closeFilterBlocks()),
        clearInputs: () => dispatch(actions.TableFilterInputActions.clearInputs())
    }
}
  
export default connect(
    null,
    mapDispatchToProps
)(CloseBtn)
