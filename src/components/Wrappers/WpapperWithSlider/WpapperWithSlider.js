import React from 'react';
import "./WpapperWithSlider.scss";

const WpapperWithSlider = ({children}) => (
    <div className="wrapper-witn-slider" id="slider">
        {children}
    </div>
);

export default WpapperWithSlider;