import React from 'react';
import './TableSection.scss';

import {connect} from 'react-redux';
import {actions} from 'actions';

import {TablePagination} from './TablePagination/TablePagination.js';
import {Table} from './Table/Table.js';
import AddBtn from 'components/EditBtns/AddBtn';

const TableSection = (props) => (
    <section className="table-section section">
        <TablePagination
            paging={props.currentPaging}
            onPageSizeSelect={props.onPageSizeSelect(props.table, props.currentPaging.pageNumber)}
            onPageClick={props.onPageClick(props.table, props.currentPaging.pageSize)}
        />
        <div className="table-wrapper">
            <Table
                table={props.table}
                entity={props.entity}
                data={props.data}
                user={props.user}
                currentSorting={props.currentSorting}
                tableFilterBlockVisibility={props.tableFilterBlockVisibility}
                tableFilterInput={props.tableFilterInput}
                onEditClick={props.onEditClick}
                onDeleteClick={props.onDeleteClick}
            />
        </div>
        
        {/* {props.user.isAdmin &&
            <AddBtn text="Добавить элемент" onClick={props.onAddClick(props.entity, props.entityId)}/>
        } */}
    </section>
);

//export default TableSection;

const mapDispatchToProps = dispatch => ({
    onAddClick: (entity, entityId) => () => {
        dispatch(actions.AdminPanelActions.openOverlay("add", entity, entityId));
    },
    onEditClick: (entity, entityId) => () => {
        dispatch(actions.AdminPanelActions.openOverlay("edit", entity, entityId));
    },
    onDeleteClick: (entity, entityId) => () => {
        dispatch(actions.AdminPanelActions.openOverlay("delete", entity, entityId));
    },
    onPageSizeSelect: (table, currentPageNumber) => (pageSize) => {
        dispatch(actions.DataActions.pageData(table, pageSize, currentPageNumber));
    },
    onPageClick: (table, currentPageSize) => (pageNumber) => {
        dispatch(actions.DataActions.pageData(table, currentPageSize, pageNumber));
    }
})

export default connect(null, mapDispatchToProps)(TableSection);

