import React from 'react';
import "./WrapperNarrow.scss";

const WrapperNarrow = ({children}) => (
    <div className="wrapper-narrow">
        {children}
    </div>
);

export default WrapperNarrow;