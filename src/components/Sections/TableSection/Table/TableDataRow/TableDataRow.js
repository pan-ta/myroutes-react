import React from 'react';
import './TableDataRow.scss';

import {TableDataCell} from './TableDataCell/TableDataCell.js';

const TableDataRow = (props) => {
    const dataRow = [];
    
    props.cells.forEach((cell, i) => {
        Object.keys(props.dataElem).forEach(key => {
            if (cell.prop === key) {
                dataRow.push(
                    <TableDataCell
                        content={props.dataElem[cell.prop]}
                        link={cell.link}
                        id={props.dataElem.id}
                        key={i}
                        edit={cell.edit}
                        user={props.user}
                        onEditClick={props.onEditClick}
                        onDeleteClick={props.onDeleteClick}
                        entity={props.entity}
                        entityId={props.entityId}
                    />
                )
            }
        })
        
    });

    return(
        <tr className="table__data-row table__row">
            {dataRow}
        </tr>
    )
};

export {TableDataRow};

