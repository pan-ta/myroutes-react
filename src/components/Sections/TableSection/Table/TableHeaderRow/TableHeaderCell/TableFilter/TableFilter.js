import React from 'react';
import './TableFilter.scss';

import {TableInput} from './TableInput/TableInput.js';
import CloseBtn from './CloseBtn/CloseBtn.js';


const TableFilter = ({inputValue, onInputChange}) => {
    return(
        <div className="table__filter-block" id="{filter-block-id}" hidden>
            <TableInput value={inputValue} onInputChange={onInputChange} />
            <CloseBtn />
        </div>
    )
};

export {TableFilter};

