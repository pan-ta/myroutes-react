
import {OPEN_LOGIN} from '../actions/AuthPopupActions';
import {OPEN_RESTORE} from '../actions/AuthPopupActions';
import {OPEN_REGISTER} from '../actions/AuthPopupActions';
import {SKIP_REGISTRATION} from '../actions/AuthPopupActions';

export const initialState = {
    isVisibleLogin: true, 
    isVisibleRestorePass: false, 
    isVisibleRegister: false, 
}
  
export function authPopupReducer(state = initialState, action) {
    switch (action.type) {
        case OPEN_LOGIN:
            return { ...state, isVisibleLogin: true, isVisibleRestorePass: false, isVisibleRegister: false}

        case OPEN_RESTORE:
            return { ...state, isVisibleLogin: false, isVisibleRestorePass: true, isVisibleRegister: false}    
    
        case OPEN_REGISTER:
            return { ...state, isVisibleLogin: false, isVisibleRestorePass: false, isVisibleRegister: true}

        case SKIP_REGISTRATION:
            return { ...state, isVisibleLogin: true, isVisibleRestorePass: false, isVisibleRegister: false}

        default:
            return state
    }
}