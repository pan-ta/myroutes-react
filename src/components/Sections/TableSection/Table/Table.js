import React from 'react';
import './Table.scss';

import {TableHeaderRow} from './TableHeaderRow/TableHeaderRow.js';
import {TableDataRow} from './TableDataRow/TableDataRow.js';

import tableParams from './tableParams.js';



const Table = (props) => {
    
    const params=tableParams[props.table];
    const tBody = [];

    props.data.forEach(elem => {
        tBody.push(
            <TableDataRow 
                entity={props.entity}
                entityId={elem.id}
                dataElem={elem} 
                key={elem.id} 
                cells={params.cells} 
                user={props.user}
                onEditClick={props.onEditClick}
                onDeleteClick={props.onDeleteClick}
            />
        )
    });

    return(
        <table className="table">
            <thead>
                <TableHeaderRow 
                    headings={params.headings} 
                    table={props.table} 
                    currentSorting={props.currentSorting} 
                    tableFilterBlockVisibility={props.tableFilterBlockVisibility} 
                    tableFilterInput={props.tableFilterInput}/>
            </thead>
            <tbody>
                {tBody} 
            </tbody>
        </table>
    )
}

export {Table};
