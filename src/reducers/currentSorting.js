import {CHANGE_CURRENT_SORTING} from 'actions/CurrentSortingActions';
import {CANCEL_SORTING} from 'actions/CurrentSortingActions';
import update from 'immutability-helper';

export const initialState = {
    routes: {
        fullName: null, //asc, desc
        firstStop: null, 
        lastStop: null, 
    }, 
    stops: {
        name: null, //asc, desc
        address: null, 

    },
    transport: {
        license: null, //asc, desc
        transportType: null, 
        model: null, 
        seats: null,
        route: null,
    }
}
    

  
export function currentSortingReducer (state = initialState, action) {
    
    switch (action.type) {
        case CHANGE_CURRENT_SORTING:
            console.log("меняем текущий параметр сортировки в сторе, обнуляем остальные");
            let updatedState = {};
            updatedState[action.payload.table] = {};
            Object.keys(state[action.payload.table]).forEach(key => {
                if (key === action.payload.prop) {
                    console.log(key);
                    updatedState[action.payload.table][key] = {
                        $set: action.payload.newValue
                    };
                } else {
                    console.log(key);
                    updatedState[action.payload.table][key] = {
                        $set: null
                    };
                }
            });

            return update(state, updatedState);

        case CANCEL_SORTING:
            console.log("обнуляем сортировку");
            return initialState;
            
        default:
            return state
    }
}