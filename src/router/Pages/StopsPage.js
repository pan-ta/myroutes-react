import React, {Fragment} from 'react';
import {connect} from 'react-redux';

import PageWrapper from 'components/Wrappers/PageWrapper/PageWrapper';
import WrapperWide from 'components/Wrappers/WrapperWide/WrapperWide';
import WrapperNarrowWhite from 'components/Wrappers/WrapperNarrow/WrapperNarrowWhite';
import Header from 'components/Header/Header';
import EditSection from 'components/Sections/EditSection/EditSection';
import IntroSection from 'components/Sections/IntroSection/IntroSection';
import TableSection from 'components/Sections/TableSection/TableSection';
import SpacerWhite from 'components/Spacer/SpacerWhite.js';
import Footer from 'components/Footer/Footer.js';

import {getPagedData} from 'utils';

const StopsPage = (props) => (
    <Fragment>
        <PageWrapper>
            <Header user={props.user} dropdowns={props.dropdowns}/>
            <WrapperWide>
                <WrapperNarrowWhite>
                    <EditSection
                        user={props.user}
                        adminPanel={props.adminPanel}
                        data={props.dataStops}
                    />
                    <IntroSection
                        user={props.user}
                        title={"Stops"} 
                        desc={"Here you can find a list of stops covered by routes of BRUGGE BRI & LL. Find detailed information about the stop by clicking on the appropriate links. For quick search use the sort and filter buttons located in the header of the table."} 
                    />
                    <TableSection 
                        table="stops"
                        data={props.dataStops}
                        user={props.user}
                        currentSorting={props.currentSorting.stops}
                        currentPaging={props.currentPaging}
                        tableFilterBlockVisibility={props.tableFilterBlockVisibility.stops}
                        tableFilterInput={props.tableFilterInput.stops}
                        adminPanel={props.adminPanel}
                        />
                </WrapperNarrowWhite>
            </WrapperWide>
            <SpacerWhite />
            <Footer />
        </PageWrapper>
    </Fragment>
)

// export default RoutesPage;

const mapStateToProps = store => {
    console.log(store);
    return {
        user: store.user,
        dropdowns: store.dropdowns,
        dataStops: getPagedData(store.data.stops, store.currentPaging.stops),
        currentSorting: store.currentSorting,
        currentPaging: {
            ...store.currentPaging.stops,
            totalPages: Math.ceil(store.data.stops.length / store.currentPaging.stops.pageSize)
        },
        tableFilterBlockVisibility: store.tableFilterBlockVisibility,
        tableFilterInput: store.tableFilterInput,
        adminPanel: store.adminPanel

    };
}

export default connect(mapStateToProps)(StopsPage);
// export default connect(mapStateToProps)(RoutesPage);