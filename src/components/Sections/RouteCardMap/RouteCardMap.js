import React from 'react';
import './RouteCardMap.scss';
import RouteMap from './RouteMap';

const RouteCardMap = ({route, stops}) => {

    return (
        <section className="route-card-map section">
            <div className="route-card-map__map">
                <RouteMap route={route} stops={stops}/>
            </div>
        </section>
    )
};

export default RouteCardMap;

