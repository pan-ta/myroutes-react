import React, {Fragment} from 'react';
import {connect} from 'react-redux';


import PageWrapper from 'components/Wrappers/PageWrapper/PageWrapper';
import WrapperWide from 'components/Wrappers/WrapperWide/WrapperWide';
import WrapperNarrowWhite from 'components/Wrappers/WrapperNarrow/WrapperNarrowWhite';
import Header from 'components/Header/Header';
import EditSection from 'components/Sections/EditSection/EditSection';
import IntroSection from 'components/Sections/IntroSection/IntroSection';
import {DetailsSectionStopCard} from 'components/Sections/DetailsSection/DetailsSectionStopCard';
import StopTimeTable from 'components/Sections/StopTimeTable/StopTimeTable';
import StopCardMap from 'components/Sections/StopCardMap/StopCardMap';
import SpacerWhite from 'components/Spacer/SpacerWhite.js';
import Footer from 'components/Footer/Footer.js';




const StopCard = (props) => {
    const stopId = +props.match.params.stopId; 
    let currentStop = null;
    props.data.stops.forEach(stop => {
        if(stop.id === stopId) {
            currentStop = stop;
        }
    });

    
    return (
        <Fragment>
            <PageWrapper>
                <Header user={props.user} dropdowns={props.dropdowns}/>
                <WrapperWide>
                    <WrapperNarrowWhite>
                        {!currentStop && 
                            <div className="warning">Stop deleted or never existed</div>
                        }
                        {currentStop && 
                            <Fragment>
                                <EditSection user={props.user} adminPanel={props.adminPanel} data={props.data.stops} />
                                <IntroSection
                                    user={props.user}
                                    entity={"stop"}
                                    entityId={currentStop.id}
                                    title={`Stop "${currentStop.name}"`} 
                                    desc={"Here you can find information about this stop, see its location on the map and get acquainted with the schedule of routes."} 
                                />
                                <DetailsSectionStopCard 
                                    address={currentStop.address}
                                    routes={currentStop.routes.join(", ")}
                                    />
                                <StopTimeTable 
                                    timeTable={currentStop.timeTable}
                                />
                                <StopCardMap stop={currentStop} />
                            </Fragment>}
                    </WrapperNarrowWhite>
                </WrapperWide>
                <SpacerWhite />
                <Footer />
            </PageWrapper>
        </Fragment>
    )
};



const mapStateToProps = store => {
    console.log(store);
    return {
        user: store.user,
        dropdowns: store.dropdowns,
        data: store.data,
        adminPanel: store.adminPanel
    };
}

export default connect(mapStateToProps)(StopCard);