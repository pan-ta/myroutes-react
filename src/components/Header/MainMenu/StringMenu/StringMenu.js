import React from 'react';
import './StringMenu.scss';

import {StringMenuItem} from './StringMenuItem/StringMenuItem.js';


const StringMenu = () => {
    return(
        <ul className="header__menu-list">
            <StringMenuItem link={"transport"} text={"Vehicles"} />
            <StringMenuItem link={"routes"} text={"Routes"} />
            <StringMenuItem link={"stops"} text={"Stops"} />
            <StringMenuItem link={"map"} text={"Find route"} />
        </ul>
    )
};

export {StringMenu};

