import React from 'react';
import './Auth.scss';
import {connect} from 'react-redux';
import {actions} from 'actions';
import users from 'users/users';
import PopupOrange from 'components/Popup/PopupOrange';
import TextInput from 'components/Forms/TextInput';
import SubmitBtn from 'components/Forms/SubmitBtn';
import {NavLink} from 'react-router-dom';

// props: 
// authPopup, 
// onLoginBtnClick, 
// onRestorePassBtnClick, 
// onRegisterBtnClick, 
// onSkipRegistrationBtnClick,
// authorizeAsAdmin
// authorizeAsUser

const Auth = (props) => {
    const onAuthSubmit = e => {
        e.preventDefault();        
        
        for(const user of users) {
            if (e.target.login.value === user.login) {
                if (e.target.password.value === user.pass) {
                    if(user.isAdmin) {
                        props.authorizeAsAdmin(user.login, user.pass, user.name, user.lastname);
                    } else {
                        props.authorizeAsUser(user.login, user.pass, user.name, user.lastname);
                    }

                    props.history.push('/');
                    return;
                }

                alert("Wrong password");
                return;
            }
        }

        alert("No user with such e-mail found")
    }

    return (
        <div className="auth">
            <PopupOrange>
                {props.authPopup.isVisibleLogin && 
                    <div className="auth__login">
                        <form
                            className="auth__login-form form"
                            id="login-form"
                            onSubmit={onAuthSubmit}
                        >
                            <TextInput 
                                type="email" 
                                name={"login"} 
                                placeholder={"E-mail"} 
                                className={"text-input auth__text-input"}
                                isRequired={true} />
                            <TextInput 
                                type={"password"} 
                                name={"password"} 
                                placeholder={"Password"} 
                                className={"text-input auth__text-input"}
                                isRequired={true} />
                            <SubmitBtn id="login-submit-btn" text="Log in" />
                        </form>
                        <div className="auth__bottom-block">
                            <button className="auth__forget" onClick={props.onRestorePassBtnClick} >Forgot your password?</button>
                            <div className="auth__wanna-register">Not registered yet? <button onClick={props.onRegisterBtnClick}>Register now</button></div>
                            <div className="auth__socials">
                                <span>Log in with:</span>
                                <ul className="auth__socials-list">
                                    <li className="auth__socials-item">
                                        <a href="index" className="auth__socials-link">
                                            <i className="fab fa-vk"></i>
                                        </a>
                                    </li>
                                    <li className="auth__socials-item">
                                        <a href="route-card" className="auth__socials-link">
                                            <i className="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <button className="auth__skip" onClick={props.onSkipRegistrationBtnClick}>
                                <NavLink
                                    exact
                                    to="/">
                                    Skip authorization
                                </NavLink>
                            </button>
                        </div>
                    </div>
                }
                {props.authPopup.isVisibleRestorePass && 
                    <div className="auth__restore-pass">
                        <div className="auth__title">Restore password</div>
                        <form action="" className="auth__restore-pass-form form" id="restore-pass-form"
                            onSubmit={props.onRestorePassSubmit}>
                            <TextInput 
                                type="email" 
                                name={"login"} 
                                placeholder={"Ваш E-mail"} 
                                className={"text-input auth__text-input"}
                                isRequired={true} />
                            <SubmitBtn id="restore-pass-submit-btn" text="Отправить" />
                        </form>
                        <div className="auth__bottom-block">
                            <span>You will be sent a link to restore access to the system.</span>
                        </div>
                        <button className="auth__back" onClick={props.onLoginBtnClick} >Back to authorization</button>
                    </div>
                }
                {props.authPopup.isVisibleRegister && 
                    <div className="auth__register">
                        <div className="auth__title">Registration</div>
                        <form action="" className="auth__restore-pass-form form" id="register-form"
                            onSubmit={props.onRegistrationSubmit}>
                            <TextInput 
                                type="text" 
                                name={"name"} 
                                placeholder={"Name (optional)"} 
                                className={"text-input auth__text-input"}
                                isRequired={false} />
                            <TextInput 
                                type="text" 
                                name={"lastname"} 
                                placeholder={"Lastname (optional)"} 
                                className={"text-input auth__text-input"}
                                isRequired={false} />
                            <TextInput 
                                type="email" 
                                name={"email"} 
                                placeholder={"E-mail"} 
                                className={"text-input auth__text-input"}
                                isRequired={true} />
                            <TextInput 
                                type="password" 
                                name={"password"} 
                                placeholder={"Password"} 
                                className={"text-input auth__text-input"}
                                isRequired={true} />
                            <SubmitBtn id="register-submit-btn" text="Register" />
                        </form>
                        <button className="auth__back" onClick={props.onLoginBtnClick} >Back to authorization</button>
                    </div>
                }
            </PopupOrange>
        </div>
    )
};

// export default Auth;

const mapDispatchToProps = dispatch => ({
    onLoginBtnClick: () => {
        dispatch(actions.AuthPopupActions.onLoginBtnClick());
    },
    onRestorePassBtnClick: () => {
        dispatch(actions.AuthPopupActions.onRestorePassBtnClick());
    },
    onRegisterBtnClick: () => {
        dispatch(actions.AuthPopupActions.onRegisterBtnClick());
    },
    onSkipRegistrationBtnClick: () => {
        dispatch(actions.AuthPopupActions.onSkipRegistrationBtnClick());
    },
    authorizeAsAdmin: (login, pass, name, lastname) => {
        dispatch(actions.UserActions.authorizeAsAdmin(login, pass, name, lastname));
    },
    authorizeAsUser: (login, pass, name, lastname) => {
        dispatch(actions.UserActions.authorizeAsUser(login, pass, name, lastname));
    },
})

export default connect(null, mapDispatchToProps)(Auth);
