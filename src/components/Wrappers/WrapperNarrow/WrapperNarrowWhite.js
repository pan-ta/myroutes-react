import React from 'react';
import "./WrapperNarrow.scss";

const WrapperNarrowWhite = ({children}) => (
    <div className="wrapper-narrow wrapper-narrow_white">
        {children}
    </div>
);

export default WrapperNarrowWhite;