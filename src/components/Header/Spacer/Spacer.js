import React from 'react';
import './Spacer.scss';


const Spacer = () => {
    return(
        <div className="spacer"></div>
    )
};

export {Spacer};

