import React, {Fragment} from 'react';
import './NewsSection.scss';

import {connect} from 'react-redux';
import {actions} from 'actions';

import SmallEditBtn from 'components/EditBtns/SmallEditBtn';
import SmallDeleteBtn from 'components/EditBtns/SmallDeleteBtn';
import AddBtn from 'components/EditBtns/AddBtn';

const NewsSection = ({user, onAddClick, onEditClick, onDeleteClick}) => (
    <section className="news section">
        <h1 className="news__title title">News</h1>
        <ul className="news__list">
            <li className="news__item">
                <div className="news__img"></div>
                <div className="news__desc">
                    <div className="news__date">07.10.2019</div>
                    <div className="news__subtitle">Electro-mobility: Greening European transport</div>
                    <div className="news__text">Electro-mobility is slowly gaining market share for passenger cars, but the EU economy, for which transport is a critical sector contributing to growth and jobs, will need multiple technologies for decades to come, experts at the European Business Summit in Brussels told summit organiser Euronews.</div>
                </div>
                {/* {user.isAdmin &&
                    <Fragment>
                        <SmallEditBtn onClick={onEditClick}/>
                        <SmallDeleteBtn onClick={onDeleteClick}/>
                    </Fragment>
                } */}
                
            </li>
            <li className="news__item">
                <div className="news__img"></div>
                <div className="news__desc">
                    <div className="news__date">07.10.2019</div>
                    <div className="news__subtitle">Madrid bans scooters on pavements</div>
                    <div className="news__text">Autumn has arrived in Madrid, bringing the first rains of the season and dying the streets with orange and grey mud. With the downpours, the cars return and the traffic jams. But this year another ingredient has become prominent: electric scooters, the latest trend in urban mobility vehicles, can be found in nearly every corner of the capital’s centre.</div>
                </div>
                {/* {user.isAdmin &&
                    <Fragment>
                        <SmallEditBtn onClick={onEditClick}/>
                        <SmallDeleteBtn onClick={onDeleteClick}/>
                    </Fragment>
                } */}
            </li>
            <li className="news__item">
                <div className="news__img"></div>
                <div className="news__desc">
                    <div className="news__date">05.10.2019</div>
                    <div className="news__subtitle">Free public transport is launched across Estonia</div>
                    <div className="news__text">Estonia is set to implement free transport for its residents across much of the country as of July 1. The free fare zone will run 24 hours a day, seven days a week. No country has ever before looked to abolish fares all day, every day, across such a large area.</div>
                </div>
                {/* {user.isAdmin &&
                    <Fragment>
                        <SmallEditBtn onClick={onEditClick}/>
                        <SmallDeleteBtn onClick={onDeleteClick}/>
                    </Fragment>
                } */}
            </li>
            <li className="news__item">
                <div className="news__img"></div>
                <div className="news__desc">
                    <div className="news__date">22.09.2019</div>
                    <div className="news__subtitle">UK brings major rail franchise back under government control</div>
                    <div className="news__text">One of the UK's rail franchises is to be brought back under government control for the third time in just over ten years.

The concession is currently run jointly by Virgin Trains and Stagecoach. They will have to hand over control on June 24.</div>
                </div>
                {/* {user.isAdmin &&
                    <Fragment>
                        <SmallEditBtn onClick={onEditClick}/>
                        <SmallDeleteBtn onClick={onDeleteClick}/>
                    </Fragment>
                } */}
            </li>
        </ul>
        <a href="1" className="news__controls">Find more</a>
        {/* {user.isAdmin &&
            <AddBtn text="Добавить новость" onClick={onAddClick}/>
        } */}
        
    </section>
);

//export default NewsSection;

const mapDispatchToProps = dispatch => ({
    onAddClick: () => {
        dispatch(actions.AdminPanelActions.openOverlay("news", "add"));
    },
    onEditClick: () => {
        dispatch(actions.AdminPanelActions.openOverlay("news", "edit"));
    },
    onDeleteClick: () => {
        dispatch(actions.AdminPanelActions.openOverlay("news", "delete"));
    }
})

export default connect(null, mapDispatchToProps)(NewsSection);
