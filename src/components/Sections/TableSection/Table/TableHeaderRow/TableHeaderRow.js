import React from 'react';
import './TableHeaderRow.scss';

import TableHeaderCell from './TableHeaderCell/TableHeaderCell.js';


const TableHeaderRow = (props) => {
    const tableHeader = [];
    props.headings.forEach(heading => {
        tableHeader.push(
            <TableHeaderCell
                key={heading.id}
                prop={heading.id}
                text={heading.name}
                ifSortable={heading.sortable}
                ifFilterable={heading.filterable}
                table={props.table}
                currentSorting={props.currentSorting}
                tableFilterBlockVisibility={props.tableFilterBlockVisibility}
                tableFilterInput={props.tableFilterInput}
            />)
    })
    return(
        <tr className="table__header-row table__row">
            {tableHeader}
        </tr>
    )
};

export {TableHeaderRow};

