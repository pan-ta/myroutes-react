import React from 'react';
import './TableHeaderCell.scss';

import {connect} from 'react-redux';
import {actions} from 'actions';
import SortBtn from './SortBtn/SortBtn.js';
import FilterBtn from './FilterBtn/FilterBtn.js';
import {TableFilter} from './TableFilter/TableFilter.js';

const TableHeaderCell = (props) => {

    const onInputChange = e => {
        props.listenToInputAction(props.table, props.prop, e.target.value);
        props.filterDataAction(props.table, props.prop, e.target.value);
    }
    
    return(
        <th className="table__header-cell table-cell">
            <div className="table__header-main">
                <div className="table__header-text">{props.text}</div>
                <div className="table__header-controls">
                {props.ifSortable && 
                    <SortBtn table={props.table} prop={props.prop} currentSorting={props.currentSorting} />}
                {props.ifFilterable && 
                    <FilterBtn table={props.table} prop={props.prop} tableFilterBlockVisibility={props.tableFilterBlockVisibility} />}    
                </div>
            </div>
            {props.ifFilterable && props.tableFilterBlockVisibility[props.prop] &&
                <TableFilter inputValue={props.tableFilterInput[props.prop]} onInputChange={onInputChange} />}
        </th>
    )
};

const mapDispatchToProps = dispatch => {
    return {
        listenToInputAction: (table, prop, inputValue) => dispatch(actions.TableFilterInputActions.listenToTableFilterInput(table, prop, inputValue)),
        filterDataAction: (table, prop, inputValue) => dispatch(actions.DataActions.filterData(table, prop, inputValue))
    }
}
  
export default connect(
    null,
    mapDispatchToProps
)(TableHeaderCell)

