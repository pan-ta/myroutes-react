import React from 'react';
import './UpsertVehicle.scss';
import {connect} from 'react-redux';
import {actions} from 'actions';


const UpsertVehicle = ({entityId, vehicles, upsertVehicle, closeOverlay}) => {

    let title;
    let btnText;
    let thisVehicle;
    if (entityId) {
        vehicles.forEach(vehicle => {
            if (vehicle.id === +entityId) {
                thisVehicle = vehicle;
            }
        });

        title = `Edit vehicle ${thisVehicle.license}`;
        btnText = "Save";
    } else {
        title = "Add vehicle";
        btnText = "Add new vehicle";
    }

    const onSubmit = (e) => {

        e.preventDefault();
        if (entityId) {
            upsertVehicle("edit", e.target.id.value, e.target.license.value, e.target.model.value, e.target.transportType.value, e.target.seats.value, e.target.route.value);
            closeOverlay();
        } else {
            upsertVehicle("add", e.target.id.value, e.target.license.value, e.target.model.value, e.target.transportType.value, e.target.seats.value, e.target.route.value);
            closeOverlay();
        }
    }

    return (
    <form className="admin__form" onSubmit={onSubmit}>
        <div className="admin__title">{title}</div>
        <div className="form-container">
            <div className="admin__wrap">
                <div className="admin__block admin__block_wide">
                    <input 
                        type="text" 
                        className="admin__input admin__input_wide" 
                        name="id" 
                        placeholder="Vehicle ID" 
                        defaultValue = {entityId ? thisVehicle.id : null}
                        />
                    <div className="admin__desc">Vehicle ID</div>
                </div>
                <div className="admin__block admin__block_wide">
                    <input type="text" 
                        className="admin__input admin__input_wide" 
                        name="license" 
                        placeholder="Vehicle license" 
                        defaultValue = {entityId ? thisVehicle.license : null}
                        />
                    <div className="admin__desc">Vehicle license</div>
                </div>
                <div className="admin__block admin__block_wide">
                    <input type="text" 
                    className="admin__input admin__input_wide" 
                    name="model" 
                    placeholder="Vehicle model" 
                    defaultValue = {entityId ? thisVehicle.model : null}
                    />
                    <div className="admin__desc">Vehicle model</div>
                </div>
                <div className="admin__block admin__block_wide">
                    <select name="transportType" 
                    className="admin__select" 
                    defaultValue = {entityId ? thisVehicle.transportType : null}>
                        <option value="Bus">Bus</option>
                        <option value="Tram">Tram</option>
                        <option value="Train">Taxi</option>
                    </select>
                    <div className="admin__desc">Vehicle transport type</div>
                </div>
                <div className="admin__block admin__block_wide">
                    <input type="text" 
                        className="admin__input admin__input_wide" 
                        name="seats" 
                        placeholder="Number of seats"
                        defaultValue = {entityId ? thisVehicle.seats : null}
                        />
                    <div className="admin__desc">Number of seats</div>
                </div>
                <div className="admin__block admin__block_wide">
                    <input type="text" 
                    className="admin__input admin__input_wide" 
                    name="route" 
                    placeholder="Serves route" 
                    defaultValue = {entityId ? thisVehicle.route : null}
                    />
                    <div className="admin__desc">Serves route</div>
                </div>
            </div>
        </div>
        <button type="submit" className="bubmit-button button">{btnText}</button> 
    </form>
)};


const mapDispatchToProps = dispatch => ({
    upsertVehicle: (task, id, license, model, transportType, seats, route) => {
        dispatch(actions.DataActions.upsertVehicle(task, id, license, model, transportType, seats, route));
    },
    closeOverlay: () => {
        dispatch(actions.AdminPanelActions.closeOverlay());
    }
})

export default connect(null, mapDispatchToProps)(UpsertVehicle);

