import { actions } from ".";

export const SORT_ROUTES = 'SORT_ROUTES';
export const SORT_ROUTES_CANCEL = 'SORT_ROUTES_CANCEL';


export function onRouteSortBtnClick(propToSort, isAsc) {
    return {
        type: SORT_ROUTES,
        payload: {
            propToSort: propToSort,
            isAsc: isAsc
        }
    }
}

export function onFiterBtnClick() {
    return {
        type: SORT_ROUTES_CANCEL,
        payload: null
    }
}
