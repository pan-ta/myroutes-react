import React from 'react';
import './Contacts.scss';




const Contacts = () => {
    return(
        <div className="contacts" id="contacts"> 
            <div className="contacts__company">
                <span>JSC transport company BRUGGE BRI & LL</span>
                <span>Hooistraat, 55, Brugge, West Flanders, Belgium</span>
            </div>
            <address className="contacts__phone-email">
                <a className="contacts__phone" href="tel:+79217425364">8 (921) 742-53-64</a>
                <a className="contacts__email" href="mailto:pan-ta@yandex.ru">info@brugge-bri.bg</a>
            </address>
        </div>
    )
};

export {Contacts};

