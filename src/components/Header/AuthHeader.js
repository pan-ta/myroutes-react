import React from 'react';
import './Header.scss';

import WrapperNarrow from 'components/Wrappers/WrapperNarrow/WrapperNarrow.js';
import {Logo} from './Logo/Logo.js';
import {Spacer} from './Spacer/Spacer.js';
import {LangMenu} from './LangMenu/LangMenu.js';


const AuthHeader = ({user}) =>(
    <header className="header header_auth" id="header"> 
        <WrapperNarrow>
            <div className="header__content">
                <Logo />
                <Spacer />
                <div className="header__menu-wrapper">
                    <LangMenu />
                </div>
            </div>
        </WrapperNarrow>
    </header>
);

export default AuthHeader;

