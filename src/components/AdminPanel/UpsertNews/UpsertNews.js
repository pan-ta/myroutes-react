import React from 'react';
import './UpsertNews.scss';



const UpsertNews = ({entityId}) => {
    let title;
    let btnText;
    if (entityId) {
        title = `Редактировать новость ${entityId}`;
        btnText = "Сохранить";
    } else {
        title = "Добавить новость";
        btnText = "Добавить новость";
    }

    return (
    <form className="admin__form">
        <div className="admin__title">{title}</div>
        <div className="form-container">
            <div className="admin__nowrap">
                <div className="admin__block admin__block_extrawide">
                    <div className="admin__desc admin__desc_up">Название новости</div>
                    <input 
                        type="text" 
                        className="admin__input admin__input_extrawide" 
                        name="route-id" 
                        placeholder="Название" 
                        // defaultValue="пер.Каховского" 
                        // onInput={onInput} 
                        />
                </div>
                <div className="admin__block admin__block_extrawide">
                    <div className="admin__desc admin__desc_up">Краткий текст новости</div>
                    <textarea className="admin__textarea" name="" id="" rows="3" cols="80"  placeholder="Краткий текст новости"></textarea>
                </div>
                <div className="admin__block admin__block_extrawide">
                    <div className="admin__desc admin__desc_up">Текст новости</div>
                    <textarea className="admin__textarea" name="" id=""  rows="6" cols="80" placeholder="Текст новости"></textarea>
                </div>
                <div className="admin__block admin__block_extrawide">
                    <input type="file" accept="image/*" />
                    <div className="admin__desc"></div>
                </div>
            </div>
        </div>
        <button type="submit" className="bubmit-button button">{btnText}</button> 
    </form>
)};


export default UpsertNews;

