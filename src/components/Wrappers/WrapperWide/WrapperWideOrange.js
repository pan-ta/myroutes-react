import React from 'react';
import "./WrapperWideOrange.scss";


const WrapperWideOrange = ({children}) => (
    <div className="wrapper-wide_orange">
        {children}
    </div>
);

export default WrapperWideOrange;