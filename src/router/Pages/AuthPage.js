import React, {Fragment} from 'react';
import {connect} from 'react-redux';

import PageWrapper from 'components/Wrappers/PageWrapper/PageWrapper';
import WpapperWithSlider from 'components/Wrappers/WpapperWithSlider/WpapperWithSlider';
import AuthHeader from 'components/Header/AuthHeader';
import Auth from 'components/Sections/Auth/Auth';


const AuthPage = (store) => (
    <Fragment>
        <PageWrapper>
            <AuthHeader />
            <WpapperWithSlider>
                <Auth history={store.history} authPopup={store.authPopup} />
            </WpapperWithSlider>
        </PageWrapper>
    </Fragment>
);


// export default AuthPage;

const mapStateToProps = store => {
    console.log(store);
    return {
        authPopup: store.authPopup
    };
}

export default connect(mapStateToProps)(AuthPage);