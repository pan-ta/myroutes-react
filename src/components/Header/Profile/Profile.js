import React from 'react';
import './Profile.scss';

import {connect} from 'react-redux';
import {actions} from 'actions';
import {ProfileBtn} from './ProfileBtn/ProfileBtn.js';
import {DropdownMenu} from './DropdownMenu/DropdownMenu.js';
import SignInBtn from './SignInBtn/SignInBtn.js';


const Profile = ({user, dropdowns, onProfileBtnClick}) => {
    if (!user.isLoggedIn) {
        return(
            <div className="profile">
                <SignInBtn />
            </div>
        )

    } else {
        return(
            <div className="profile">
                <ProfileBtn onClick={onProfileBtnClick} />
                {dropdowns.isVisibleProfileDropdown && 
                    <DropdownMenu user={user} />
                }
            </div>
        )
    }
};

//export {Profile};

const mapDispatchToProps = dispatch => ({
    onProfileBtnClick: () => {
        dispatch(actions.DropdownActions.onProfileBtnClick());
    }
})

export default connect(null, mapDispatchToProps)(Profile);