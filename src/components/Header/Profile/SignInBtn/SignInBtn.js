import React from 'react';
import {NavLink} from 'react-router-dom';
import './SignInBtn.scss';


const SignInBtn = () => {
    return(
        <NavLink
            to="/auth">
            <button className="profile__signin-btn signin-btn" id="signin-btn">
                <i className="fas fa-sign-in-alt"></i>
            </button>
        </NavLink>
    )
};


export default SignInBtn;