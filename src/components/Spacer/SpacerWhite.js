import React from 'react';
import './SpacerWhite.scss';
import WrapperNarrowWhite from 'components/Wrappers/WrapperNarrow/WrapperNarrowWhite';


const SpacerWhite = () => (
    <div className="spacer">
        <WrapperNarrowWhite>
            
        </WrapperNarrowWhite>
    </div>
);

export default SpacerWhite;

