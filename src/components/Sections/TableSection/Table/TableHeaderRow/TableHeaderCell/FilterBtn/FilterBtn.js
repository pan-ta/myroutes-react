import React from 'react';
import { connect } from 'react-redux';
import {actions} from 'actions';
import './FilterBtn.scss';


const FilterBtn = ({table, prop, openFilterBlockAction, cancelSortingAction, clearInputs}) => {
    
    const onThisFilterBtnClick = () => {
        clearInputs();
        openFilterBlockAction(table, prop);
        cancelSortingAction();
    };

    return(
        <button onClick={onThisFilterBtnClick} className="filter-btn" id="filter-btn">
            <i className="fas fa-filter"></i>
        </button>
    )
};

// export {FilterBtn};

const mapDispatchToProps = dispatch => {
    return {
        clearInputs: () => dispatch(actions.TableFilterInputActions.clearInputs()),
        openFilterBlockAction: (table, prop) => dispatch(actions.TableFilterBlockVisibilityActions.openFilterBlock(table, prop)),
        cancelSortingAction: () => dispatch(actions.CurrentSortingActions.cancelSorting())

    }
}

export default connect(
    null,
    mapDispatchToProps
)(FilterBtn)


