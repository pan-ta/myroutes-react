import React, {Fragment} from 'react';

import './TableDataCell.scss';
import {NavLink} from 'react-router-dom';
import SmallEditBtn from 'components/EditBtns/SmallEditBtn';
import SmallDeleteBtn from 'components/EditBtns/SmallDeleteBtn';


const TableDataCell = (props) => {
    const {user, content, link, id, edit, entityId, entity} = props;
    const {onEditClick, onDeleteClick} = props;
    let newContent = {};
    if (!link) {
        if (Array.isArray(content)) {
            newContent = content.join(", ");
        } else {
            newContent = content;
        }
        return(
            <td className="table__data-cell table-cell">
                <span>{newContent}</span>
                {user.isAdmin && edit &&
                    <Fragment>
                        <SmallEditBtn onClick={onEditClick(entity, entityId)}/>
                        <SmallDeleteBtn onClick={onDeleteClick(entity, entityId)}/>
                    </Fragment>
                }
            </td>
        )
    } else {
        const fullLink = `/${link}/${id}`;
        return(
            <td className="table__data-cell table-cell">
                <NavLink to={fullLink}>
                    {content}
                </NavLink>
                {user.isAdmin && edit &&
                    <Fragment>
                        <SmallEditBtn onClick={onEditClick(entity, entityId)}/>
                        <SmallDeleteBtn onClick={onDeleteClick(entity, entityId)}/>
                    </Fragment>
                }
            </td>
        )
    }
}


export {TableDataCell};

