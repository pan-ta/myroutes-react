import React from 'react';
import './PageAmountSelect.scss';


const PageAmountSelect = ({value, onSelect}) => {
    const onSelectChange = e => onSelect(+e.target.value);
    return(
        <div className="pagination__page-amount">
            <span>Show entries: </span>
            <select
                className="pagination__page-amount-select select"
                value={value}
                onChange={onSelectChange}
            >
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>

            </select>
        </div>
    )
};

export {PageAmountSelect};

