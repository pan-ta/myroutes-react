import React from 'react';
import "./WrapperWide.scss";


const WrapperWide = ({children}) => (
    <div className="wrapper-wide">
        {children}
    </div>
);

export default WrapperWide;