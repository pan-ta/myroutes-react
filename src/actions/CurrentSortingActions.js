
export const CHANGE_CURRENT_SORTING = 'CHANGE_CURRENT_SORTING';
export const CANCEL_SORTING = 'CANCEL_SORTING';

export function changeCurrentSorting(table, prop, newValue) {
    return {
        type: CHANGE_CURRENT_SORTING,
        payload: {
            table: table,
            prop: prop,
            newValue: newValue
        }
    }
}

export function cancelSorting() {
    return {
        type: CANCEL_SORTING,
        payload: null
    }
}