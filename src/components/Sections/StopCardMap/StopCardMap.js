import React from 'react';
import './StopCardMap.scss';
import StopMap from './StopMap';

const StopCardMap = ({stop}) => {

    return (
        <section className="stop-card-map section">
            <div className="stop-card-map__map">
                <StopMap stop={stop}/>
            </div>
        </section>
    )
};

export default StopCardMap;

