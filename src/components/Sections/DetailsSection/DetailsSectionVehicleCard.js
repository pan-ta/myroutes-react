import React from 'react';
import './DetailsSection.scss';



const DetailsSectionVehicleCard = ({model, seats, route}) => {
    return(
        <div className="details-section section">
            <div className="details-section__string">
                <span>Vehicle model: </span>
                <span>{model}</span>
            </div>
            <div className="details-section__string">
                <span>Number of seats: </span>
                <span>{seats}</span>
            </div>
            <div className="details-section__string">
                <span>Covers the route: </span>
                <span>{route}</span>
            </div>
        </div>
    )
};

export {DetailsSectionVehicleCard};

