import React from 'react';

import './DropdownMenu.scss';

import {DropdownMenuItem} from './DropdownMenuItem/DropdownMenuItem.js';
import LogoutBtn from './LogoutBtn/LogoutBtn.js';

const DropdownMenu = ({user}) => {
    return(
        <div className="dropdown profile__dropdown" id="profile-dropdown">
            <div className="dropdown__arrow"></div>
            <div className="dropdown__title">
                {user.name && user.lastname &&
                    <span>{`${user.name} ${user.lastname}`}</span> }

                {(!user.name || !user.lastname) &&
                    <span>{user.login}</span> }
            </div>
            <ul className="dropdown__menu-list">
                <DropdownMenuItem link={"favourites-routes"} text={"My routes"} />
                <DropdownMenuItem link={"favourites-stops"} text={"My stops"} />
            </ul>
            <div className="dropdown__extras">
                <LogoutBtn />
            </div>
        </div>
    )
};

export {DropdownMenu};