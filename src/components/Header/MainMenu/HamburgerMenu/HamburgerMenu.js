import React from 'react';
import './HamburgerMenu.scss';

import HamburgerBtn from './HamburgerBtn/HamburgerBtn.js';
import {DropdownMenu} from './DropdownMenu/DropdownMenu.js';


const HamburgerMenu = ({dropdowns}) => {
    return(
        <div className="hamburger" id="hamburger">
            <HamburgerBtn />
            <DropdownMenu dropdowns={dropdowns} />
        </div>
    )
};

export {HamburgerMenu};

