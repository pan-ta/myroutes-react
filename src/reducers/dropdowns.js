
import { OPEN_HAMBURGER, CLOSE_HAMBURGER, OPEN_PROFILE, CLOSE_PROFILE } from '../actions/DropdownsActions';

export const initialState = {
    isVisibleHamburgerDropdown: false, 
    isVisibleProfileDropdown: false, 
}
  
export function dropdownsReducer(state = initialState, action) {
    switch (action.type) {
        case OPEN_HAMBURGER:
            return { ...state, isVisibleHamburgerDropdown: true}
        case CLOSE_HAMBURGER:
            return { ...state, isVisibleHamburgerDropdown: false}
        case OPEN_PROFILE:
            return { ...state, isVisibleProfileDropdown: true}
        case CLOSE_PROFILE:
            return { ...state, isVisibleProfileDropdown: false}
    
        default:
            return state
    }
}