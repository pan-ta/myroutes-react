import { PAGE_DATA, SORT_DATA, FILTER_DATA } from 'actions/DataActions';
import update from 'immutability-helper';

export const initialState = {
    routes: {
        pageNumber: 1,
        pageSize: 10
    }, 
    stops: {
        pageNumber: 1,
        pageSize: 10
    },
    transport: {
        pageNumber: 1,
        pageSize: 10
    }
}
 
export function currentPagingReducer (state = initialState, action) {
    
    let updatedState = {};

    switch (action.type) {
        case PAGE_DATA:
            console.log("меняем текущую страницу пейджинга");
            updatedState[action.payload.table] = {};
            updatedState[action.payload.table].pageSize = { $set: action.payload.pageSize }

            if(action.payload.pageSize !== state[action.payload.table].pageSize) {
                updatedState[action.payload.table].pageNumber = {
                    $set: initialState[action.payload.table].pageNumber
                }    
            } else {
                updatedState[action.payload.table].pageNumber = {
                    $set: action.payload.pageNumber
                }
            }

            return update(state, updatedState);
        
        case SORT_DATA:
        case FILTER_DATA:
            
            console.log("сбрасываем первую страницу пейджинга");
            updatedState[action.payload.table] = {};
            updatedState[action.payload.table].pageNumber = {
                $set: initialState[action.payload.table].pageNumber
            }    

            return update(state, updatedState);

        default:
            return state
    }
}