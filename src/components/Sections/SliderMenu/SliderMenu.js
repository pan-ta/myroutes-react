import React from 'react';
import {NavLink} from 'react-router-dom';
import './SliderMenu.scss';

import { Fade } from 'react-slideshow-image';
import Autocomplete from 'react-autocomplete';
 
const fadeProperties = {
  duration: 7000,
  transitionDuration: 500,
  infinite: true,
  indicators: false,
  arrows: false
}

const Slideshow = () => {
    return (
        <Fade {...fadeProperties}>
            <div className="each-fade each-fade_1"></div>
            <div className="each-fade each-fade_2"></div>
            <div className="each-fade each-fade_3"></div>
            <div className="each-fade each-fade_4"></div>
            <div className="each-fade each-fade_5"></div>
        </Fade>
    )
}

export default class SliderMenu extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            item1: { label: '' },
            item2: { label: '' }

        }
    }

    

    render() {
        
        const stopsObjs=[];
            
        this.props.data.stops.forEach(stop => {
            let newObj = {};
            newObj.id = stop.id;
            newObj.label = stop.name;
            stopsObjs.push(newObj)
        });

        console.log(this.state);

        const getValidStopsItems = (oppositeValueInState) => {

            const oppositeValue = this.state[oppositeValueInState];

            if(!oppositeValue.id) {
                return stopsObjs;
            }

            const resultStopsObjs = [];
            this.props.data.routes.forEach(route => {

                const routeWithOppositeValue = route.stops.find(stopId => stopId === oppositeValue.id);

                if(!routeWithOppositeValue) {
                    return;
                }

                route.stops.forEach(stopId => {
                    if (stopId === oppositeValue.id)
                    {
                        return;
                    }
                    
                    if (!resultStopsObjs.some(stop => stop.id === stopId))
                    {
                        resultStopsObjs.push(stopsObjs.find(stop => stop.id === stopId));
                    };
                });
            });

            console.log(resultStopsObjs);

            return resultStopsObjs;
        }

        const onAutocompleteChange = (valueInState) => (e) => {
            const itemLabel = e.target.value;
            const itemByName = stopsObjs.find(s => s.label === itemLabel);
            const itemId = itemByName ? itemByName.id : null;

            const newState = {};
            newState[valueInState] = {
                label: itemLabel,
                id: itemId
            }
            this.setState({ ...this.state, ...newState });
        }

        const onSubmit = (e) => {
            e.preventDefault();
            // setDataForBigMap();
            this.props.history.push('/map');
        }

        return (
            <section className="slider-menu">
                <div className="slider-menu__slides">
                    <Slideshow />
                </div>
                <div className="slider-menu__menu">
                    <form className="slider-menu__form" onSubmit={onSubmit}>
                        <div className="slider-menu__inputs">
                            {/* <input type="text" className="text-input" placeholder="Откуда?"/> */}
                            <Autocomplete
                                items={getValidStopsItems("item2")}
                                shouldItemRender={(item, value) => item.label.toLowerCase().indexOf(value.toLowerCase()) > -1}
                                getItemValue={item => item.label}
                                renderItem={(item, highlighted) =>
                                    <div
                                        key={item.id}
                                        style={{ backgroundColor: highlighted ? '#eee' : 'transparent'}}
                                    >
                                        {item.label}
                                    </div>}
                                value={this.state.item1.label}
                                onChange={onAutocompleteChange("item1")}
                                onSelect={(value, item) => this.setState({ item1: item })}

                                inputProps={{
                                    placeholder: "From"
                                }}
                                menuStyle={{
                                    zIndex: "100",
                                    boxShadow: '2px 2px 5px 3px rgba(0,0,0, 0.2)',
                                    background: 'rgba(255, 255, 255, 1)',
                                    padding: '10px',
                                    fontSize: '16px',
                                    position: 'fixed',
                                    overflow: 'auto',
                                    maxHeight: '50%', // TODO: don't cheat, let it flow to the bottom
                                }}
                            />
                            <i className="fas fa-angle-double-right"></i>
                            {/* <input type="text" className="text-input" placeholder="Куда?"/> */}
                            <Autocomplete
                                items={getValidStopsItems("item1")}
                                shouldItemRender={(item, value) => item.label.toLowerCase().indexOf(value.toLowerCase()) > -1}
                                getItemValue={item => item.label}
                                renderItem={(item, highlighted) =>
                                    <div
                                        key={item.id}
                                        style={{ backgroundColor: highlighted ? '#eee' : 'transparent'}}
                                    >
                                        {item.label}
                                    </div>}
                                value={this.state.item2.label}
                                onChange={onAutocompleteChange("item2")}
                                onSelect={(value, item) => this.setState({ item2: item })}

                                inputProps={{
                                    placeholder: "To"
                                }}
                                menuStyle={{
                                    width: "200px",
                                    boxShadow: '2px 2px 5px 3px rgba(0,0,0, 0.2)',
                                    background: 'rgba(255, 255, 255, 1)',
                                    padding: '10px',
                                    fontSize: '16px',
                                    position: 'fixed',
                                    overflow: 'auto',
                                    maxHeight: '50%', // TODO: don't cheat, let it flow to the bottom
                                }}
                            />
                        </div>    
                        <button type="submit">Go!</button>

                    </form>
                    <div className="slider-menu__buttons">
                        <button className="slider-menu__button">
                            <NavLink to="/transport">
                                <div className="slider-menu__button-content">
                                    <span><i className="fas fa-bus"></i></span>
                                    <span>Vehicles</span>
                                </div>
                            </NavLink>
                        </button>
                        <button className="slider-menu__button">
                            <NavLink to="/routes">
                                <div className="slider-menu__button-content">
                                    <span><i className="fas fa-route"></i></span>
                                    <span>Routes</span>
                                </div>
                            </NavLink>
                        </button>
                        <button className="slider-menu__button">
                            <NavLink to="/stops">
                                <div className="slider-menu__button-content">
                                    <span><i className="fas fa-city"></i></span>
                                    <span>Stops</span>
                                </div>
                            </NavLink>
                        </button>
                        <button className="slider-menu__button">
                            <NavLink to="/map">
                                <div className="slider-menu__button-content">
                                    <span><i className="fas fa-map-marked-alt"></i></span>
                                    <span>Map</span>
                                </div>
                            </NavLink>
                        </button>
                    </div>
                </div>
            </section>
        )
    }
}