import React from 'react';
import './PagesList.scss';

import {PagesItem} from './PagesItem/PagesItem.js';


const PagesList = ({currentPage, totalPages, onClick}) => {

    const getPages = () => {
        const pages = [];

        for (let i = 1; i <= totalPages; i++) {
            pages.push(
                <PagesItem
                    key={i}
                    pageNumber={i}
                    isCurrent={i === currentPage}
                    onClick={onClick}
                />
            );
        }

        return pages;
    }

    return(
        <ul className="pagination__pages-list">{ getPages() }</ul>
    )
};

export {PagesList};

