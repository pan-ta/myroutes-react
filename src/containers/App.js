import React, { Component } from 'react';
import './App.scss'; 
import 'scss/normalize.css'; 

import Router from 'router/Router.js';

class App extends Component {
    render() {
        return (
            <div className="App-container">
                <Router />
            </div>
        );
    }
}

export default App;
