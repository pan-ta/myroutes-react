import React from 'react';


class RouteMap extends React.Component {

    componentDidMount() {
        window.ymaps.ready(this.init.bind(this));
    }

    getReferencePoint = () => {
        return [
            this.props.stop.coord1,
            this.props.stop.coord2
        ]
    };


    init = () => {

        var myMap = new window.ymaps.Map('map', {
            center: this.getReferencePoint(),
            zoom: 15,
            controls: ['smallMapDefaultSet']
            }, {
            });

        myMap.geoObjects
            .add(new window.ymaps.Placemark(this.getReferencePoint(), {
                iconCaption: this.props.stop.name,
                
            }, {
                preset: 'islands#circleIcon',
                iconColor: '#CD7233'
            }));
        }
    

    render() {
        return <div id="map" style={{ "height" : "50vh" }}></div>
    }
}

export default RouteMap;