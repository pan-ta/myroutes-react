import React from 'react';
import './ValidationDropdown.scss';


const ValidationDropdown = ({text}) => {
    return(
        <div className="validation-dropdown" hidden>
            <div className="validation-dropdown__arrow"></div>
            <div className="validation-dropdown__text">{text}</div>
        </div>
    )
};

export {ValidationDropdown};

