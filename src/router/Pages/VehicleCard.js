import React, {Fragment} from 'react';
import {connect} from 'react-redux';




import PageWrapper from 'components/Wrappers/PageWrapper/PageWrapper';
import WrapperWide from 'components/Wrappers/WrapperWide/WrapperWide';
import WrapperNarrowWhite from 'components/Wrappers/WrapperNarrow/WrapperNarrowWhite';
import Header from 'components/Header/Header';
import EditSection from 'components/Sections/EditSection/EditSection';
import IntroSection from 'components/Sections/IntroSection/IntroSection';
import {DetailsSectionVehicleCard} from 'components/Sections/DetailsSection/DetailsSectionVehicleCard';
import VehiclePhoto from 'components/Sections/VehiclePhoto/VehiclePhoto';
import SpacerWhite from 'components/Spacer/SpacerWhite.js';
import Footer from 'components/Footer/Footer.js';




const VehicleCard = (props) => {
    const vehicleId = +props.match.params.vehicleId; 
    let currentVehicle = null;
    props.data.transport.forEach(transport => {
        if(transport.id === vehicleId) {
            currentVehicle = transport;
        }
    });

    
    return (
        <Fragment>
            <PageWrapper>
                <Header user={props.user} dropdowns={props.dropdowns}/>
                <WrapperWide>
                    <WrapperNarrowWhite>
                        {!currentVehicle && 
                            <div className="warning">Vehicle deleted or never existed</div>
                        }
                        {currentVehicle && 
                            <Fragment>
                                <EditSection user={props.user} adminPanel={props.adminPanel} data={props.data.transport} />
                                <IntroSection
                                    user={props.user}
                                    entity={"vehicle"}
                                    entityId={currentVehicle.id}
                                    title={`${currentVehicle.transportType} ${currentVehicle.license}`} 
                                    desc={"Here you can find information about this vehicle, check out yhe model, license and number of seats."} 
                                />
                                <DetailsSectionVehicleCard 
                                    model={currentVehicle.model}
                                    seats={currentVehicle.seats}
                                    route={currentVehicle.route}
                                />
                                <VehiclePhoto vehicle={currentVehicle.model}/>
                            </Fragment>}
                    </WrapperNarrowWhite>
                </WrapperWide>
                <SpacerWhite />
                <Footer />
            </PageWrapper>
        </Fragment>
    )
};



const mapStateToProps = store => {
    console.log(store);
    return {
        user: store.user,
        dropdowns: store.dropdowns,
        data: store.data,
        adminPanel: store.adminPanel
    };
}

export default connect(mapStateToProps)(VehicleCard);