import React, {Fragment} from 'react';
import {NavLink} from 'react-router-dom';
import './StopsListSection.scss';



const StopsListSection = ({stopsNames, stopsId}) => {
    const stopsItems=[];
    
    stopsNames.forEach((stopName, i) => {
        const link = `/stop-card/${stopsId[i]}`;
        stopsItems.push(
            <Fragment key={i}>
                <li className="stops__item">
                    <span>{String.fromCharCode(65+i)}. </span>
                    <NavLink
                        to={link}
                        style={{textDecoration: 'none'}}
                        >
                        {stopName}
                    </NavLink>
                    <i className="fas fa-angle-double-right"></i>
                </li>
            </Fragment>
        )
    });

    return(
        <div className="stops section">
            <ul className="stops__list">
                {stopsItems}
            </ul>
        </div>
    )
};

export {StopsListSection};

