import React from 'react';
import './CloseBtn.scss';




const CloseBtn = ({onClick}) => (
    <button className="close-btn" onClick={onClick}>
        <i className="fas fa-times"></i>
    </button>
);


export default CloseBtn;

