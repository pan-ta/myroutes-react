import React from 'react';
import './DetailsSection.scss';



const DetailsSectionStopCard = ({address, routes}) => {
    return(
        <div className="details-section section">
            <div className="details-section__string">
                <span>Stop address: </span>
                <span>{address}</span>
            </div>
            {/* <div className="details-section__string">
                <span>Проходящие маршруты: </span>
                <span>{routes}</span>
            </div> */}
        </div>
    )
};

export {DetailsSectionStopCard};

