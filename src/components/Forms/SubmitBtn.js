import React from 'react';
import './Button.scss';



const SubmitBtn = ({id, text}) => (
    <button type="submit" className="button" id={id}>{text}</button>
);

export default SubmitBtn;

