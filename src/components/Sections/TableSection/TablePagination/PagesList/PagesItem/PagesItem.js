import React from 'react';
import './PagesItem.scss';


const PagesItem = ({pageNumber, isCurrent, onClick}) => {
    return(
        <li className="pagination__pages-item">
            <a
                className="pagination__pages-link"
                onClick={isCurrent ? () => null : () => onClick(pageNumber)}
            >{pageNumber}</a>
        </li>
    )
};

export {PagesItem};

