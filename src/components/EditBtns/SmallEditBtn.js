import React from 'react';
import './SmallBtn.scss';




const SmallEditBtn = ({onClick}) => (
    <button className="small-btn small-btn_edit" onClick={onClick}>
        <i className="fas fa-edit"></i>
    </button>
);


export default SmallEditBtn;

