import { LOGGED_OUT, LOGGED_IN_AS_USER, LOGGED_IN_AS_ADMIN } from 'actions/UserActions'

export const initialState = {
    isLoggedIn: false, 
    isAdmin: false,
    login: null,
    pass: null,
    name: null,
    lastname: null
}
  
export function userReducer(state = initialState, action) {
    switch (action.type) {

        case LOGGED_IN_AS_USER:
            return {...state, isLoggedIn: true, isAdmin: false, login: action.payload.login, pass: action.payload.pass, name: action.payload.name, lastname: action.payload.lastname}

        case LOGGED_IN_AS_ADMIN:
            return {...state, isLoggedIn: true, isAdmin: true, login: action.payload.login, pass: action.payload.pass, name: action.payload.name, lastname: action.payload.lastname}

        case LOGGED_OUT:
            return initialState
    
        default:
            return state
    }
}