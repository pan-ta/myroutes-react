
import { OPEN_OVERLAY, CLOSE_OVERLAY } from '../actions/AdminPanelActions';

export const initialState = {
    isOverlayOpen: false, 
    task: null, // edit, add, delete
    entity: null, // stop, route, vehicle, news
    entityId: null, 
    
}
  
export function adminPanelReducer(state = initialState, action) {
    switch (action.type) {
        case OPEN_OVERLAY:
            return {
                ...state,
                isOverlayOpen: true,
                task: action.payload.task,
                entity: action.payload.entity,
                entityId: action.payload.entityId,
                
            }
        
        case CLOSE_OVERLAY:
            return initialState
        
        default:
            return state
    }
}