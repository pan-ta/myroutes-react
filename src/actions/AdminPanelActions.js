
export const OPEN_OVERLAY = 'OPEN_OVERLAY';
export const CLOSE_OVERLAY = 'CLOSE_OVERLAY';



export function openOverlay(task, entity, entityId) {
    return {
        type: OPEN_OVERLAY,
        payload: {
            task: task,
            entity: entity,
            entityId: entityId,
            
            
        }
    }
}

export function closeOverlay() {
    return {
        type: CLOSE_OVERLAY,
        payload: null
    }
}
