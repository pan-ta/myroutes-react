import React from 'react';
import './VehiclePhoto.scss';


const VehiclePhoto = ({vehicle}) => {
    let newClassName; 
    
    switch(vehicle) {
        case 'Volvo FMX':  
            newClassName = 'VolvoFMX';
            break;

        case 'Ford Transit':  
            newClassName = 'FordTransit';
            
            break;

        case 'YUTONG':  
            newClassName = 'YUTONG';
            break;
      
        default:
            newClassName = 'defaultVehicle';
            break;
    }

    return (
        <section className="vehicle-photo">
            <div className={newClassName}></div>
        </section>
)};

export default VehiclePhoto;

