
export const SORT_ROUTES_BY_FIRST_STOP = 'SORT_ROUTES_BY_FIRST_STOP';


export function onFirstStopSortBtnClick(currentSorting) {
    return {
        type: SORT_ROUTES_BY_FIRST_STOP,
        payload: currentSorting
    }
}
