import React from 'react';
import './Spacer.scss';


const Spacer = () => (

    <div className="spacer content-wrapper"></div>
);

export default Spacer;

