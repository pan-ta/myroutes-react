import React from 'react';
import './Header.scss';

import WrapperNarrow from 'components/Wrappers/WrapperNarrow/WrapperNarrow.js';
import {Logo} from './Logo/Logo.js';
import {Spacer} from './Spacer/Spacer.js';
import {LangMenu} from './LangMenu/LangMenu.js';
import {MainMenu} from './MainMenu/MainMenu.js';
import Profile from './Profile/Profile.js';



const Header = ({user, dropdowns}) => (
    <header className="header header_main" id="header"> 
        <WrapperNarrow>
            <div className="header__content">
                <Logo />
                <Spacer />
                <div className="header__menu-wrapper">
                    <LangMenu />
                    <MainMenu dropdowns={dropdowns} />
                </div>
                <Profile user={user} dropdowns={dropdowns} />
            </div>
        </WrapperNarrow>
    </header>
);

export default Header;

