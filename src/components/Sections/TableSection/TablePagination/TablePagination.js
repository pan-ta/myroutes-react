import React from 'react';
import './TablePagination.scss';

import {PageAmountSelect} from './PageAmountSelect/PageAmountSelect.js';
import {CurrentPageNmb} from './CurrentPageNmb/CurrentPageNmb.js';
import {PagesList} from './PagesList/PagesList.js';



const TablePagination = ({paging, onPageSizeSelect, onPageClick}) => {
    return(
        <div className="pagination">
            <PageAmountSelect value={paging.pageSize} onSelect={onPageSizeSelect} />
            { paging.totalPages !== 0 &&
                <CurrentPageNmb
                    currentPage={paging.pageNumber}
                    totalPages={paging.totalPages}
                /> }
            { paging.totalPages > 1 &&
                <PagesList
                    currentPage={paging.pageNumber}
                    totalPages={paging.totalPages}
                    onClick={onPageClick}
                /> }
        </div>
    )
};

export {TablePagination};

