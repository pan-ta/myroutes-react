
export const OPEN_FILTER_BLOCK_AND_CLOSE_OTHERS = 'OPEN_FILTER_BLOCK_AND_CLOSE_OTHERS'; 
export const CLOSE_ALL_FILTER_BLOCKS = 'CLOSE_ALL_FILTER_BLOCKS';

export function openFilterBlock(table, prop) {
    return {
        type: OPEN_FILTER_BLOCK_AND_CLOSE_OTHERS,
        payload: {
            table: table,
            prop: prop,
        }
    }
}

export function closeFilterBlocks() {
    return {
        type:  CLOSE_ALL_FILTER_BLOCKS,
        payload: null
    }
}