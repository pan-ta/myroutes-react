import React from 'react';
import './TextInput.scss';



const TextInput = ({type, name, placeholder, className, isRequired}) => (
    <input 
        type={type} 
        name={name} 
        placeholder={placeholder} 
        className={className} 
        required={isRequired} >
    </input>
);

export default TextInput;

