import React from 'react';
import './DropdownMenuItem.scss';


const DropdownMenuItem = ({link, text}) => {
    return(
        <li className="dropdown__menu-item">
            <a className="dropdown__menu-link" href={link}>{text}</a>
        </li>
    )
};

export {DropdownMenuItem};

