
export const LISTEN_TO_TABLE_FILTER_INPUT = 'LISTEN_TO_TABLE_FILTER_INPUT';
export const CLEAR_INPUTS = 'CLEAR_INPUTS';

export function listenToTableFilterInput(table, prop, inputValue) {
    return {
        type: LISTEN_TO_TABLE_FILTER_INPUT,
        payload: {
            table: table,
            prop: prop,
            inputValue: inputValue
        }
    }
}

export function clearInputs() {
    return {
        type: CLEAR_INPUTS,
        payload: null
    }
}