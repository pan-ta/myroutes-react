import React from 'react';
import './LangMenu2.scss';

const LangMenu = () => {
    return(
        <ul className="header__language-list header__language-list_hamb">
            <li className="header__language-item">
                <a href="1" className="header__language-link">Rus</a>
            </li>
            <li className="header__language-item">
                <a href="2" className="header__language-link">Eng</a>
            </li>
        </ul>
    )
};

export {LangMenu};

