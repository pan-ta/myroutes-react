export const EDIT_MODE_ON = 'EDIT_MODE_ON';
export const EDIT_MODE_OFF = 'EDIT_MODE_OFF';


export function onAdminLoggedIn() {
    return {
        type: EDIT_MODE_ON,
        payload: null
    }
  }

  export function onUserLoggedgOut() {
    return {
        type: EDIT_MODE_OFF,
        payload: null
    }
  }