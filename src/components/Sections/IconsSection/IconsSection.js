import React from 'react';
import './IconsSection.scss';


const IconsSection = () => (

    <section className="icons section">
        <ul className="icons__list">
            <li className="icons__item">
                <a href="transport" className="icons__link">
                    <i className="fas fa-bus"></i>
                </a>
                <div className="icons__desc">Транспорт</div>
            </li>
            <li className="icons__item">
                <a href="routes" className="icons__link">
                    <i className="fas fa-route"></i>
                </a>
                <div className="icons__desc">Маршруты</div>
            </li>
            <li className="icons__item">
                <a href="stops" className="icons__link">
                    <i className="fas fa-city"></i>
                </a>
                <div className="icons__desc">Остановки</div>
            </li>
            <li className="icons__item">
                <a href="map" className="icons__link">
                    <i className="fas fa-map-marked-alt"></i>
                </a>
                <div className="icons__desc">Построить маршрут</div>
            </li>
        </ul>
    </section>
);

export default IconsSection;

