import React from 'react';
import "./PageWrapper.scss";

import {connect} from 'react-redux';
import {actions} from 'actions/index'
import {onOutsideClick} from 'utils';

const PageWrapper = (props) => {

    const onWrapperClick = e => {
        if(!props.dropdowns) {
            return;
        }
        
        onOutsideClick(
            e,
            props.dropdowns.isVisibleHamburgerDropdown,
            "hamburger-dropdown",
            props.onHamburgerClose);

        onOutsideClick(
            e,
            props.dropdowns.isVisibleProfileDropdown,
            "profile-dropdown",
            props.onProfileClose);
    }

    return (
        <div className="page-wrapper" id="page-wrapper" onClick={onWrapperClick}>
            {props.children}
        </div>
    )
}

//export default PageWrapper;

const mapStateToProps = store => ({ dropdowns: store.dropdowns });

const mapDispatchToProps = dispatch => ({
    onHamburgerClose: () => dispatch(actions.DropdownActions.onHamburgerClose()),
    onProfileClose: () => dispatch(actions.DropdownActions.onProfileClose())
})

export default connect(mapStateToProps, mapDispatchToProps)(PageWrapper);