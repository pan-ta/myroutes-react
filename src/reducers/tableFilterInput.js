import {LISTEN_TO_TABLE_FILTER_INPUT, CLEAR_INPUTS} from 'actions/TableFilterInputActions';
import update from 'immutability-helper';

export const initialState = {
    routes: {
        fullName: "",
        firstStop: "", 
        lastStop: "", 
    }, 
    stops: {
        name: "", 
        address: "", 

    },
    transport: {
        license: "", 
        transportType: "", 
        model: "", 
        route: "", 
    }
}
    

  
export function tableFilterInputReducer (state = initialState, action) {
    
    switch (action.type) {
        case LISTEN_TO_TABLE_FILTER_INPUT:
            console.log("событие на инпуте таблицы");
            if (action.payload.inputValue !== null) {
                let updatedState = {};
                updatedState[action.payload.table] = {};
                updatedState[action.payload.table][action.payload.prop] = {
                    $set: action.payload.inputValue
                };
                return update(state, updatedState);
            } else {
                return initialState
            }

        case CLEAR_INPUTS:
            console.log("обнуляем инпуты");
            return initialState
            
        default:
            return state
    }
}