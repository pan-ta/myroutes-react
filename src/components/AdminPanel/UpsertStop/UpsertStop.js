import React from 'react';
import './UpsertStop.scss';
import {connect} from 'react-redux';
import {actions} from 'actions';


const UpsertStop = ({entityId, stops, upsertStop, closeOverlay}) => {

    let title;
    let btnText;
    let thisStop;
    if (entityId) {
        stops.forEach(stop => {
            if (stop.id === +entityId) {
                thisStop = stop;
            }
        });

        title = `Edit stop ${thisStop.name}`;
        btnText = "Save";
    } else {
        title = "Add stop";
        btnText = "Add new stop";
    }

    const onSubmit = (e) => {

        e.preventDefault();
        if (entityId) {
            upsertStop("edit", e.target.id.value, e.target.name.value, e.target.address.value, e.target.coord1.value, e.target.coord2.value);
            closeOverlay();
        } else {
            upsertStop("add", e.target.id.value, e.target.name.value, e.target.address.value, e.target.coord1.value, e.target.coord2.value);
            closeOverlay();
        }
    }

    return (
    <form className="admin__form" onSubmit={onSubmit}>
        <div className="admin__title">{title}</div>
        <div className="form-container">
            <div className="admin__wrap">
                <div className="admin__block admin__block_wide">
                    <input type="text" 
                    className="admin__input admin__input_wide" 
                    name="id" 
                    placeholder="Stop ID" 
                    defaultValue = {entityId ? thisStop.id : null}
                    />
                    <div className="admin__desc">Stop ID</div>
                </div>
                <div className="admin__block admin__block_wide">
                    <input 
                        type="text" 
                        className="admin__input admin__input_wide" 
                        name="name" 
                        placeholder="Stop name" 
                        defaultValue = {entityId ? thisStop.name : null}
                        />
                    <div className="admin__desc">Stop name</div>
                </div>
                <div className="admin__block admin__block_wide">
                    <input type="text" 
                    className="admin__input admin__input_wide" 
                    name="address" 
                    placeholder="Stop address" 
                    defaultValue = {entityId ? thisStop.address : null}
                    />
                    <div className="admin__desc">Stop address</div>
                </div>
                <div className="admin__block admin__block_wide">
                    <input type="text" 
                    className="admin__input admin__input_wide" 
                    name="coord1" 
                    placeholder="Сoordinates (lat)"  
                    defaultValue = {entityId ? thisStop.coord1 : null}
                    />
                    <div className="admin__desc">Сoordinates (lat)</div>
                </div>
                <div className="admin__block admin__block_wide">
                    <input type="text" 
                    className="admin__input admin__input_wide" 
                    name="coord2" 
                    placeholder="Сoordinates (lon)" 
                    defaultValue = {entityId ? thisStop.coord2 : null}
                    />
                    <div className="admin__desc">Сoordinates (lon)</div>
                </div>
            </div>
        </div>
        <button type="submit" className="bubmit-button button">{btnText}</button> 
    </form>
)};


const mapDispatchToProps = dispatch => ({
    upsertStop: (task, id, name, address, coord1, coord2) => {
        dispatch(actions.DataActions.upsertStop(task, id, name, address, coord1, coord2));
    },
    closeOverlay: () => {
        dispatch(actions.AdminPanelActions.closeOverlay());
    }
})

export default connect(null, mapDispatchToProps)(UpsertStop);

