import {SORT_ASC_BY_ROUTE_NAME} from '../actions/RoutesTableSorting';
import {SORT_DESC_BY_ROUTE_NAME} from '../actions/RoutesTableSorting';
import {SORT_ASC_BY_FIRST_STOP} from '../actions/RoutesTableSorting';
import {SORT_DESC_BY_FIRST_STOP} from '../actions/RoutesTableSorting';
import {SORT_ASC_BY_LAST_STOP} from '../actions/RoutesTableSorting';
import {SORT_DESC_BY_LAST_STOP} from '../actions/RoutesTableSorting';
import {CANCEL_SORTING} from '../actions/RoutesTableSorting';

export const initialState = {
    currentSortingRouteName: null, // asc, desc
    currentSortingFirstStop: null, // asc, desc
    currentSortingLastStop: null, // asc, desc

}
  
export function routesTableSortingReducer(state = initialState, action) {
    switch (action.type) {
        case SORT_ASC_BY_ROUTE_NAME:
            return {...state, currentSortingRouteName: "asc", currentSortingFirstStop: null, currentSortingLastStop: null}
        case SORT_DESC_BY_ROUTE_NAME:
            return {...state, currentSortingRouteName: "desc", currentSortingFirstStop: null, currentSortingLastStop: null}
        case SORT_ASC_BY_FIRST_STOP:
            return {...state, currentSortingRouteName: null, currentSortingFirstStop: "asc", currentSortingLastStop: null}
        case SORT_DESC_BY_FIRST_STOP:
            return {...state, currentSortingRouteName: null, currentSortingFirstStop: "desc", currentSortingLastStop: null}
        case SORT_ASC_BY_LAST_STOP:
            return {...state, currentSortingRouteName: null, currentSortingFirstStop: null, currentSortingLastStop: "asc"}
        case SORT_DESC_BY_LAST_STOP:
            return {...state, currentSortingRouteName: null, currentSortingFirstStop: null, currentSortingLastStop: "desc"}
        case CANCEL_SORTING:
            return {...state, currentSortingRouteName: null, currentSortingFirstStop: null, currentSortingLastStop: null}
        

        default:
            return state
    }
}

