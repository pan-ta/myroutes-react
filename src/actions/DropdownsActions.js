
export const OPEN_HAMBURGER = 'OPEN_HAMBURGER';
export const OPEN_PROFILE = 'OPEN_PROFILE';
export const CLOSE_HAMBURGER = 'CLOSE_HAMBURGER';
export const CLOSE_PROFILE = 'CLOSE_PROFILE';


export function onHamburgerBtnClick() {
    return {
        type: OPEN_HAMBURGER,
        payload: null
    }
  }

  export function onHamburgerClose() {
    return {
        type: CLOSE_HAMBURGER,
        payload: null
    }
  }

  export function onProfileBtnClick() {
    return {
        type: OPEN_PROFILE,
        payload: null
    }
  }

  export function onProfileClose() {
    return {
        type: CLOSE_PROFILE,
        payload: null
    }
  }
