import {routesData} from 'data/data';
import { SORT_ROUTES } from 'actions/RoutesDataActions'
import { SORT_ROUTES_CANCEL } from 'actions/RoutesDataActions'

export const initialState = {
    routes: routesData, 

}
  
export function routesDataReducer(state = initialState, action) {
    switch (action.type) {
        case SORT_ROUTES:
            console.log("сортируем пути по параметрам");
            return (sortDataByProp(action.payload.isAsc, action.payload.propToSort, state.routes));

        case SORT_ROUTES_CANCEL:
            console.log("сбросить сортировку путей");
            return state;
    
        default:
            return state
    }
}

function sortDataByProp(data, prop, isAsc) {

    function comparePropsAsc(a, b) {
        if (a[prop] > b[prop]) return 1;
        if (a[prop] < b[prop]) return -1;
    }
    function comparePropsDesc(a, b) {
        if (a[prop] > b[prop]) return -1;
        if (a[prop] < b[prop]) return 1;
    }
    let sortedData;

    if (isAsc) {
        sortedData = data.sort(comparePropsAsc);
    } else {
        sortedData = data.sort(comparePropsDesc);
    }

    return sortedData;

}
