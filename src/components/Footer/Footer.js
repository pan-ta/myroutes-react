import React from 'react';
import './Footer.scss';

import WrapperNarrow from 'components/Wrappers/WrapperNarrow/WrapperNarrow.js';
import {Contacts} from './Contacts/Contacts.js';
import {Socials} from './Socials/Socials.js';


const Footer = () => (
    <div className="footer" id="footer"> 
        <WrapperNarrow>
            <div className="footer__content">
                <Contacts />
                <Socials />
            </div>
        </WrapperNarrow>
    </div>
);

export default Footer;

