import React, {Fragment} from 'react';
import {connect} from 'react-redux';

import PageWrapper from 'components/Wrappers/PageWrapper/PageWrapper';
import WrapperWide from 'components/Wrappers/WrapperWide/WrapperWide';
import WrapperNarrowWhite from 'components/Wrappers/WrapperNarrow/WrapperNarrowWhite';
import Header from 'components/Header/Header';
import EditSection from 'components/Sections/EditSection/EditSection';
import IntroSection from 'components/Sections/IntroSection/IntroSection';
import NewsSection from 'components/Sections/NewsSection/NewsSection';
import Spacer from 'components/Spacer/SpacerWhite';
import Footer from 'components/Footer/Footer';
import SliderMenu from 'components/Sections/SliderMenu/SliderMenu';


const HomePage = (props) => (
    <Fragment>
        <PageWrapper>
            <Header user={props.user} dropdowns={props.dropdowns} />
            <WrapperWide>
                <WrapperNarrowWhite>
                    <EditSection user={props.user} adminPanel={props.adminPanel} />
                    <SliderMenu data={props.data} history={props.history}/>
                    <IntroSection
                        user={props.user}
                        title={"myRoutes - find your route!"} 
                        desc={"Welcome to myRoutes.ru – the simple and easy way to get information about public transport in Brugge, find out the schedule, check out stops addresses and find the quickest routes to move all across the city center and suburbs. Enjoy!"} 
                    />
                    <NewsSection user={props.user} adminPanel={props.adminPanel} />
                </WrapperNarrowWhite>
            </WrapperWide>
            <Spacer />
            <Footer />
        </PageWrapper>
    </Fragment>
);

// export default HomePage;

const mapStateToProps = store => {
    console.log(store);
    return {
        user: store.user,
        dropdowns: store.dropdowns,
        data: store.data,
        adminPanel: store.adminPanel
    };
}

export default connect(mapStateToProps)(HomePage);

