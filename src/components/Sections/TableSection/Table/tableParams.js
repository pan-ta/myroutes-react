const tableParams = {
    routes: {
        headings: [{
            name: "Route",
            id: "fullName",
            sortable: true,
            filterable: true,
        },
        {
            name: "First stop",
            id: "firstStop",
            sortable: true,
            filterable: true,
        },
        {
            name: "Last stop",
            id: "lastStop",
            sortable: true,
            filterable: true,
        },
        // {
        //     name: "Crossing routes",
        //     id: "crossRoutes",
        //     sortable: false,
        //     filterable: false,
        // }
    ],
        cells: [
            {
                prop: "fullName",
                link: "route-card",
                edit: true
            },
            {
                prop: "firstStop",
                link: null,
                edit: false
            },
            {
                prop: "lastStop",
                link: null,
                edit: false
            },
            // {
            //     prop: "crossRoutes",
            //     link: null,
            //     edit: false
            // }
        ]
    },
    transport: {
        headings: [
        {
            name: "License",
            id: "license",
            sortable: true,
            filterable: true,
        },
        {
            name: "Transport type",
            id: "transportType",
            sortable: true,
            filterable: true,
        },
        {
            name: "Model",
            id: "model",
            sortable: true,
            filterable: true,
        },
        {
            name: "Number of seats",
            id: "seats",
            sortable: true,
            filterable: false,
        },
        {
            name: "Serves route",
            id: "route",
            sortable: true,
            filterable: true,
        }
    ],
        cells: [
            {
                prop: "license",
                link: "vehicle-card",
                edit: true
            },
            {
                prop: "transportType",
                link: null,
                edit: false
            },
            {
                prop: "model",
                link: null,
                edit: false
            },
            
            {
                prop: "seats",
                link: null,
                edit: false
            },
            {
                prop: "route",
                link: null,
                edit: false
            }
        ]
    },
    stops: {
        headings: [{
            name: "Name",
            id: "name",
            sortable: true,
            filterable: true,
        },
        {
            name: "Address",
            id: "address",
            sortable: true,
            filterable: true,
        },
        {
            name: "Routes",
            id: "routes",
            sortable: false,
            filterable: false,
        }],
        cells: [
            {
                prop: "name",
                link: "stop-card",
                edit: true
            },
            {
                prop: "address",
                link: null,
                edit: false
            },
            {
                prop: "routes",
                link: null,
                edit: false
            }
        ]
    }
}

export default tableParams;
