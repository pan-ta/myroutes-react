import React from 'react';
import './StringMenuItem.scss';
import {NavLink} from 'react-router-dom';

const StringMenuItem = ({link, text}) => {
    const thisLink = `/${link}`
    return(
        <li className="header__menu-item">
            <NavLink
                to={thisLink}
                // style={{textDecoration: 'none'}}
                activeClassName='active'
                >
                {text}
            </NavLink>
        </li>
    )
};

export {StringMenuItem};

