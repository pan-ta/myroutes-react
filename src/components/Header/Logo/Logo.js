import React from 'react';
import './Logo.scss';
import {NavLink} from 'react-router-dom';


const Logo = () => {
    return(
        <NavLink
            exact
            to="/"
            // activeClassName="nav-link"
            style={{textDecoration: 'none'}}>
            <div className="logo header__logo">myRoutes</div>
        </NavLink>
    )
};

export {Logo};

