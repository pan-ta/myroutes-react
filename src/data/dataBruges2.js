const initialData = {
    "stops": [
        {
            "id": 502820,
            "coord1": 51.196304, 
            "coord2": 3.214179,
            "name": "Sint-Michiels Station West",
            "address": "Sint-Michiels Station West",
            "timeTable": [
                {
                    "routeName": "40",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "40R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
            
        },
        {
            "id": 502682,
            "coord1": 51.188714, 
            "coord2": 3.201691,
            "name": "Sint-Michiels Vives",
            "address": "Sint-Michiels Vives",
            "timeTable": [
                {
                    "routeName": "40",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "40R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 507446,
            "coord1": 51.188499, 
            "coord2": 3.198730,
            "name": "Sint-Michiels Grasdreef",
            "address": "Sint-Michiels Grasdreef",
            "timeTable": [
                {
                    "routeName": "40",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "40R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                },
                {
                    "routeName": "50",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "50R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 502462,
            "coord1": 51.188179, 
            "coord2": 3.194256,
            "name": "Sint-Michiels Vogelzang",
            "address": "Sint-Michiels Vogelzang",
            "timeTable": [
                {
                    "routeName": "40",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "40R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                },
                {
                    "routeName": "60",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "60R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 502388,
            "coord1": 51.189151, 
            "coord2": 3.188554,
            "name": "Sint-Andries Diksmuidse Heerweg",
            "address": "Sint-Andries Diksmuidse Heerweg",
            "timeTable": [
                {
                    "routeName": "40",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "40R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 502578,
            "coord1": 51.190368, 
            "coord2": 3.187395,
            "name": "Sint-Andries Boomhut",
            "address": "Sint-Andries Boomhut",
            "timeTable": [
                {
                    "routeName": "40",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "40R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 507399,
            "coord1": 51.188909, 
            "coord2": 3.178651,
            "name": "Sint-Andries Lange Molen",
            "address": "Sint-Andries Lange Molen",
            "timeTable": [
                {
                    "routeName": "40",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "40R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 502652,
            "coord1": 51.202300, 
            "coord2": 3.193330,
            "name": "Sint-Andries Vti Zandstraat",
            "address": "Sint-Andries Vti Zandstraat",
            "timeTable": [
                {
                    "routeName": "50",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "50R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 502401,
            "coord1": 51.200609,
            "coord2": 3.196763,
            "name": "Sint-Andries Manitoba",
            "address": "Sint-Andries Manitoba",
            "timeTable": [
                {
                    "routeName": "50",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "50R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 502588,
            "coord1": 51.193849, 
            "coord2": 3.196781,
            "name": "Sint-Andries Hemelvaart",
            "address": "Sint-Andries Hemelvaart",
            "timeTable": [
                {
                    "routeName": "50",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "50R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                },
                {
                    "routeName": "60",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "60R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 519403,
            "coord1": 51.184574, 
            "coord2": 3.202189,
            "name": "ADMB Informatica",
            "address": "ADMB Informatica",
            "timeTable": [
                {
                    "routeName": "50",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "50R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 511739,
            "coord1": 51.180111, 
            "coord2": 3.200311,
            "name": "Kinepolis Bruges",
            "address": "Kinepolis Bruges",
            "timeTable": [
                {
                    "routeName": "50",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "50R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 516435,
            "coord1": 51.182050, 
            "coord2": 3.186966,
            "name": "W.O.T.C. De Berkjes vzw",
            "address": "W.O.T.C. De Berkjes vzw",
            "timeTable": [
                {
                    "routeName": "60",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "60R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 517720,
            "coord1": 51.185458, 
            "coord2": 3.190171,
            "name": "Boekeneute Dierenartsenpraktijk",
            "address": "Boekeneute Dierenartsenpraktijk",
            "timeTable": [
                {
                    "routeName": "60",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "60R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 512041,
            "coord1": 51.190588, 
            "coord2": 3.195052,
            "name": "Autobedrijf Vande Kerkhove",
            "address": "Autobedrijf Vande Kerkhove",
            "timeTable": [
                {
                    "routeName": "60",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "60R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 507589,
            "coord1": 51.195058, 
            "coord2": 3.199290,
            "name": "Sint-Andries Farao",
            "address": "Sint-Andries Farao",
            "timeTable": [
                {
                    "routeName": "60",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "60R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 507590,
            "coord1": 51.197699, 
            "coord2": 3.201833,
            "name": "Sint-Andries De Klokke",
            "address": "Sint-Andries De Klokke",
            "timeTable": [
                {
                    "routeName": "60",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "60R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 507407,
            "coord1": 51.203492, 
            "coord2": 3.208613,
            "name": "Sint-Andries Phare",
            "address": "Sint-Andries Phare",
            "timeTable": [
                {
                    "routeName": "60",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "60R",
                    "time": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        }
    ],
    "routes": [
        {
            "id": 10040,
            "name": "40",
            "stops": [502820, 502682, 507446, 502462, 502388, 502578, 507399],
            "transports": [106789, 106790, 106791, 106792],
            "transportType": "bus",

        },
        {
            "id": 11040,
            "name": "40R",
            "stops": [507399, 502578, 502388, 502462, 507446, 502682, 502820],
            "transports": [106789, 106790, 106791, 106792],
            "transportType": "bus",

        },
        {
            "id": 10050,
            "name": "50",
            "stops": [502652, 502401, 502588, 507446, 519403, 511739],
            "transports": [106789, 106790, 106791, 106792],
            "transportType": "bus",

        },
        {
            "id": 11050,
            "name": "50R",
            "stops": [511739, 519403, 507446, 502588, 502401, 502652],
            "transports": [106789, 106790, 106791, 106792],
            "transportType": "bus",

        },
        {
            "id": 10060,
            "name": "60",
            "stops": [516435, 517720, 502462, 512041, 502588, 507589, 507590, 507407],
            "transports": [106789, 106790, 106791, 106792],
            "transportType": "bus",

        },
        {
            "id": 11060,
            "name": "60R",
            "stops": [507407, 507590, 507589, 502588, 512041, 502462, 517720, 516435],
            "transports": [106789, 106790, 106791, 106792],
            "transportType": "bus",

        }
    ],
    "transport": [
        {
            "id": 106789,
            "transportType": "bus",
            "model": "YUTONG ",
            "license": "MV345H",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106790,
            "transportType": "bus",
            "model": "YUTONG",
            "license": "MV345H",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106791,
            "transportType": "bus",
            "model": "Volvo FMX",
            "license": "MV345H",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106792,
            "transportType": "bus",
            "model": "Volvo FMX",
            "license": "MV345H",
            "routeId": 10040,
            "seats": 80
        },
        {
            "id": 116792,
            "transportType": "route bus",
            "model": "Ford Transit",
            "license": "MV345H",
            "routeId": 10040,
            "seats": 40
        }
    ] 
}


const StopsData = [];
const RoutesData = [];
const TransportData = [];

initialData.stops.forEach(stop => {
    let newStop = stop;
    newStop.routes = [];
    initialData.routes.forEach(route => {
        route.stops.forEach(routeStop => {
            if (routeStop === stop.id) {
                newStop.routes.push(`${route.transportType} ${route.name}`);
            }
        })
    })
    StopsData.push(newStop);

})

initialData.routes.forEach(route => {
    let newRoute = route;
    newRoute.fullName = `${route.transportType} ${route.name}`;
    newRoute.length = route.stops.length;
    initialData.stops.forEach(stop => {
        if (stop.id === route.stops[0]) {
            newRoute.firstStop = stop.name;
        } else if (stop.id === route.stops[route.stops.length-1]) {
            newRoute.lastStop = stop.name;
        }
    })
    //crossings
    newRoute.crossRoutes = [];
    initialData.routes.forEach(neighbourRoute => {
        if (neighbourRoute.id !== route.id) {
            route.stops.forEach(stop => {
                neighbourRoute.stops.forEach(neibhbourStop => {
                    if (neibhbourStop === stop) {
                        newRoute.crossRoutes.push(neighbourRoute.name);
                        // break; ..почему нельзя?? 
                    }
                })
            })
        }
    })
    
    RoutesData.push(newRoute);

})

initialData.transport.forEach(vehicle => {
    let newVehicle = vehicle;
    initialData.routes.forEach(route => {
        if (route.id === vehicle.routeId) {
            newVehicle.route = route.name;
        } 
    })

    TransportData.push(newVehicle);

});



// console.log(RoutesData[0]);

export {TransportData};
export {RoutesData}; 
export {StopsData};