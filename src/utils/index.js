export const onOutsideClick = (e, visibleFlag, markerId, closeAction) => {
    if(!visibleFlag) {
        return;
    }

    let target = e.target;
    while (target) {
        if(target.id === markerId) {
            return;
        }
        
        target = target.parentNode;
    }

    closeAction();
}

export const getPagedData = (data, paging) => {
    const skipRows = (paging.pageNumber - 1) * paging.pageSize;
    const result = [];
    for(let i = skipRows; i < skipRows + paging.pageSize && i < data.length; i++) {
        result.push(data[i]);
    }
    return result;
}