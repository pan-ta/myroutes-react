
export const SORT_DATA = 'SORT_DATA';
export const FILTER_DATA = 'FILTER_DATA';
export const PAGE_DATA = 'PAGE_DATA';

export const DELETE_ENTITY = 'DELETE_ENTITY';
export const UPSERT_ROUTE = 'UPSERT_ROUTE';
export const UPSERT_STOP = 'UPSERT_STOP';
export const UPSERT_VEHICLE = 'UPSERT_VEHICLE';


export function sortData(table, prop, isAsc) {
    return {
        type: SORT_DATA,
        payload: {
            table: table,
            prop: prop,
            isAsc: isAsc
        }
    }
}

export function filterData(table, prop, inputValue) {
    return {
        type: FILTER_DATA,
        payload: {
            table: table,
            prop: prop,
            inputValue: inputValue
        }
    }
}

export function pageData(table, pageSize, pageNumber) {
    return {
        type: PAGE_DATA,
        payload: {
            table: table,
            pageSize: pageSize,
            pageNumber: pageNumber
        }
    }
}


export function deleteEntity(entity, id) {
    return {
        type: DELETE_ENTITY,
        payload: {
            entity: entity,
            id: id,
        }
    }
}


export function upsertRoute(task, id, name, transportType, stops) {
    return {
        type: UPSERT_ROUTE,
        payload: {
            task: task,
            id: id,
            name: name,
            transportType: transportType,
            stops: stops
        }
    }
}

export function upsertStop(task, id, name, address, coord1, coord2) {
    return {
        type: UPSERT_STOP,
        payload: {
            task: task,
            id: id,
            coord1: coord1,
            coord2: coord2,
            name: name,
            address: address,
        }
    }
}

export function upsertVehicle(task, id, license, model, transportType, seats, route) {
    return {
        type: UPSERT_VEHICLE,
        payload: {
            task: task,
            id: id,
            license: license,
            model: model,
            transportType: transportType,
            seats: seats,
            route: route,
        }
    }
}





