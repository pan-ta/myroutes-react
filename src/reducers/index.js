import { combineReducers } from 'redux'
import { userReducer } from './user'
import { dropdownsReducer } from './dropdowns'
import { authPopupReducer } from './auth-popup'
import { dataReducer } from './data';
import { currentSortingReducer } from './currentSorting';
import { currentPagingReducer } from './currentPaging';
import { tableFilterBlockVisibilityReducer } from './tableFilterBlockVisibility'; 
import { tableFilterInputReducer } from './tableFilterInput';
import { adminPanelReducer } from './adminPanel';

export const rootReducer = combineReducers({
    user: userReducer,
    dropdowns: dropdownsReducer,
    authPopup: authPopupReducer,
    data: dataReducer,
    currentSorting: currentSortingReducer,
    currentPaging: currentPagingReducer,
    tableFilterBlockVisibility: tableFilterBlockVisibilityReducer,
    tableFilterInput: tableFilterInputReducer,
    adminPanel: adminPanelReducer


  })