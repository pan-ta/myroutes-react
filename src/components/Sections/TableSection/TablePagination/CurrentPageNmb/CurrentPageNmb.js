import React from 'react';
import './CurrentPageNmb.scss';



const CurrentPageNmb = ({currentPage, totalPages}) => {
    return(
        <div className="pagination__current-page">Page {currentPage} from {totalPages}</div>
    )
};

export {CurrentPageNmb};

