import React from 'react';
import './SmallBtn.scss';




const SmallDeleteBtn = ({onClick}) => (
    <button className="small-btn small-btn_delete" onClick={onClick}>
        <i className="fas fa-trash-alt"></i>
    </button>
);


export default SmallDeleteBtn;

