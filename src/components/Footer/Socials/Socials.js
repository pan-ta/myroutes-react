import React from 'react';
import './Socials.scss';

import {SocialsItem} from './SocialsItem/SocialsItem.js';



const Socials = () => {
    return(
        <div className="socials" id="socials">
            <SocialsItem link={"123"} icon={"fab fa-vk"} />
            <SocialsItem link={"123"} icon={"fab fa-facebook-square"} />
            <SocialsItem link={"33"} icon={"fab fa-twitter"} />
        </div>
    )
};

export {Socials};

