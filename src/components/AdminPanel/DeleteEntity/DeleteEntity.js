import React from 'react';
import './DeleteEntity.scss';
import {connect} from 'react-redux';
import {actions} from 'actions';



const DeleteEntity = ({entity, entityId, deleteEntity, closeOverlay}) => {
    let entityName;
    if (entity === "route") {
        entityName = "route" // "маршрут"
    } else if (entity === "vehicle") {
        entityName = "vehicle" // "транспортное средство"
    } else if (entity === "stop") {
        entityName = "stop" //"остановку"
    }
    console.log(entity, entityId);

    const onYesClick = () => {
        deleteEntity(entity, entityId);
        closeOverlay();
    }

    const onCancelClick = () => {
        closeOverlay();
    }
 
    return (
        <div className="admin-panel__delete-entity">
            <div className="admin-panel__message">
                <i className="fas fa-exclamation-triangle"></i>
                <span>Are you sure you want to remove {entityName} #{entityId} from data?</span> 
                {/* Вы уверены, что хотите удалить {entityName} #{entityId}? */}
            </div>
            <div className="admin-panel__delete-btns">
                <button type="button" className="submit-button button" onClick={onYesClick}>Yes, remove</button>
                {/* Да, удалить Отмена*/}
                <button type="button" className="submit-button button" onClick={onCancelClick}>Cancel</button>
            </div>
        </div>
    )
};


// export default DeleteEntity;


const mapDispatchToProps = dispatch => ({
    deleteEntity: (entity, entityId) => {
        dispatch(actions.DataActions.deleteEntity(entity, entityId));
    },
    closeOverlay: () => {
        dispatch(actions.AdminPanelActions.closeOverlay());
    }
})

export default connect(null, mapDispatchToProps)(DeleteEntity);


