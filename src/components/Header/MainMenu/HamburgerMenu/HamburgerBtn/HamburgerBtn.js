import React from 'react';
import './HamburgerBtn.scss';
import {connect} from 'react-redux';
import {actions} from 'actions';


const HamburgerBtn = ({onHamburgerBtnClick}) => {
    return(
        <button className="hamburger__btn" id="hamburger-btn"
            onClick={onHamburgerBtnClick}>
            <i className="fas fa-bars"></i>
        </button>
    )
};

const mapDispatchToProps = dispatch => ({
    onHamburgerBtnClick: () => {
        dispatch(actions.DropdownActions.onHamburgerBtnClick());
    }
})

export default connect(null, mapDispatchToProps)(HamburgerBtn);

