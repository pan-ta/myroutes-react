import {OPEN_FILTER_BLOCK_AND_CLOSE_OTHERS} from 'actions/TableFilterBlockVisibilityActions';
import {CLOSE_ALL_FILTER_BLOCKS} from 'actions/TableFilterBlockVisibilityActions';

import update from 'immutability-helper';

export const initialState = {
    routes: {
        fullName: false, 
        firstStop: false, 
        lastStop: false, 
    }, 
    stops: {
        name: false, 
        address: false, 

    },
    transport: {
        license: null, 
        model: null, 
        transportType: null,
        route: null, 
    }
}
    

  
export function tableFilterBlockVisibilityReducer (state = initialState, action) {
    
    switch (action.type) {
        case OPEN_FILTER_BLOCK_AND_CLOSE_OTHERS: 
            console.log("открываем блок фильтра, закрываем остальные");
            let updatedState = {};
            updatedState[action.payload.table] = {};

            Object.keys(state[action.payload.table]).forEach(key => {
                if (key === action.payload.prop) {
                    updatedState[action.payload.table][key] = {
                        $set: true
                    };
                } else {
                    updatedState[action.payload.table][key] = {
                        $set: false
                    };
                }
            });
            return update(state, updatedState);

        case CLOSE_ALL_FILTER_BLOCKS:
            return initialState
            
        default:
            return state
    }
}