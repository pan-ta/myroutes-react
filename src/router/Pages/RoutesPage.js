import React, {Fragment} from 'react';
import {connect} from 'react-redux';

import PageWrapper from 'components/Wrappers/PageWrapper/PageWrapper';
import WrapperWide from 'components/Wrappers/WrapperWide/WrapperWide';
import WrapperNarrowWhite from 'components/Wrappers/WrapperNarrow/WrapperNarrowWhite';
import Header from 'components/Header/Header';
import EditSection from 'components/Sections/EditSection/EditSection';
import IntroSection from 'components/Sections/IntroSection/IntroSection';
import TableSection from 'components/Sections/TableSection/TableSection';
import SpacerWhite from 'components/Spacer/SpacerWhite.js';
import Footer from 'components/Footer/Footer.js';

import {getPagedData} from 'utils';

const RoutesPage = (props) => (
    <Fragment>
        <PageWrapper>
            <Header user={props.user} dropdowns={props.dropdowns}/>
            <WrapperWide>
                <WrapperNarrowWhite>
                    <EditSection
                        user={props.user}
                        adminPanel={props.adminPanel}
                        data={props.dataRoutes}
                    />
                    <IntroSection
                        user={props.user}
                        title={"Routes"} 
                        desc={"Here you can find a list of routes carried out by BRUGGE BRI & LL. Find etailed information about the route by clicking on the appropriate links. For quick search use the sort and filter buttons located in the header of the table."} 
                    />
                    <TableSection 
                        table="routes"
                        entity="route"
                        data={props.dataRoutes}
                        user={props.user}
                        currentSorting={props.currentSorting.routes}
                        currentPaging={props.currentPaging}
                        tableFilterBlockVisibility={props.tableFilterBlockVisibility.routes}
                        tableFilterInput={props.tableFilterInput.routes}
                        adminPanel={props.adminPanel}
                    />
                </WrapperNarrowWhite>
            </WrapperWide>
            <SpacerWhite />
            <Footer />
        </PageWrapper>
    </Fragment>
)

// export default RoutesPage;

const mapStateToProps = store => {
    console.log(store);
    return {
        user: store.user,
        dropdowns: store.dropdowns,
        dataRoutes: getPagedData(store.data.routes, store.currentPaging.routes),
        currentSorting: store.currentSorting,
        currentPaging: {
            ...store.currentPaging.routes,
            totalPages: Math.ceil(store.data.routes.length / store.currentPaging.routes.pageSize)
        },
        tableFilterBlockVisibility: store.tableFilterBlockVisibility,
        tableFilterInput: store.tableFilterInput,
        adminPanel: store.adminPanel

    };
}

export default connect(mapStateToProps)(RoutesPage);
// export default connect(mapStateToProps)(RoutesPage);