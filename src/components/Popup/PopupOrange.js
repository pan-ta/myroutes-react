import React from 'react';
import './Popup.scss';



const PopupOrange = ({children}) => (
    <div className="popup popup_orange">
        {children}
    </div>
);

export default PopupOrange;

