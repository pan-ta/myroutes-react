import React from 'react';
import './LogoutBtn.scss';

const LogoutBtn = () => {
    return(
        <button className="logout-btn profile__logout-btn" id="logout-btn">
            <i className="fas fa-sign-out-alt"></i>
        </button>
    )
};

export {LogoutBtn};

