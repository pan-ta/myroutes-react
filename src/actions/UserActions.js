
export const LOGGED_OUT = 'LOGGED_OUT';
export const LOGGED_IN_AS_USER = 'LOGGED_IN_AS_USER';
export const LOGGED_IN_AS_ADMIN = 'LOGGED_IN_AS_ADMIN';

export function onLogoutBtnClick() {
    return {
        type: LOGGED_OUT,
        payload: null
    }
}

export function authorizeAsAdmin(login, pass, name, lastname) {
    return {
        type: LOGGED_IN_AS_ADMIN,
        payload: {
            login: login,
            pass: pass, 
            name: name, 
            lastname: lastname, 
            
        }
    }
}

export function authorizeAsUser(login, pass, name, lastname) {
    return {
        type: LOGGED_IN_AS_USER,
        payload: {
            login: login,
            pass: pass, 
            name: name, 
            lastname: lastname, 
            
        }
    }
}
