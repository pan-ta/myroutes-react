import React from 'react';
import './TableInput.scss';

const TableInput = ({value, onInputChange}) => {
    return(
        <input
            value={value}
            onChange={onInputChange}
            type="text"
            className="text-input text-input_table"
            placeholder="Start printing"
            id="{filter-input-id}"
        />
    )
};

export {TableInput};

