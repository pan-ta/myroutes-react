import React, {Fragment} from 'react';
import {connect} from 'react-redux';


import PageWrapper from 'components/Wrappers/PageWrapper/PageWrapper';
import WrapperWithMap from 'components/Wrappers/WrapperWithMap/WrapperWithMap';
import Header from 'components/Header/Header';
import BigMap from 'components/Sections/BigMap/BigMap';




const MapPage = (props) => {

    return (
        <Fragment>
            <PageWrapper>
                <Header user={props.user} dropdowns={props.dropdowns}/>
                    <WrapperWithMap>
                        <BigMap />
                    </WrapperWithMap>
            </PageWrapper>
        </Fragment>
    )
};



const mapStateToProps = store => {
    console.log(store);
    return {
        user: store.user,
        dropdowns: store.dropdowns,
        data: store.data,
    };
}

export default connect(mapStateToProps)(MapPage);