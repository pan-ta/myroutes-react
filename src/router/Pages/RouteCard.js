import React, {Fragment} from 'react';
import {connect} from 'react-redux';
// import {actions} from 'actions'



import PageWrapper from 'components/Wrappers/PageWrapper/PageWrapper';
import WrapperWide from 'components/Wrappers/WrapperWide/WrapperWide';
import WrapperNarrowWhite from 'components/Wrappers/WrapperNarrow/WrapperNarrowWhite';
import Header from 'components/Header/Header';
import EditSection from 'components/Sections/EditSection/EditSection';
import IntroSection from 'components/Sections/IntroSection/IntroSection';
import {DetailsSectionRouteCard} from 'components/Sections/DetailsSection/DetailsSectionRouteCard';
import {StopsListSection} from 'components/Sections/StopsListSection/StopsListSection';
import RouteCardMap from 'components/Sections/RouteCardMap/RouteCardMap';
import SpacerWhite from 'components/Spacer/SpacerWhite.js';
import Footer from 'components/Footer/Footer.js';




const RouteCard = (props) => {
    const routeId = +props.match.params.routeId; 
    let currentRoute = null;
    props.data.routes.forEach(route => {
        if(route.id === routeId) {
            currentRoute = route;
        }
    });
    
    return (
        <Fragment>
            <PageWrapper>
                <Header user={props.user} dropdowns={props.dropdowns}/>
                <WrapperWide>
                    <WrapperNarrowWhite>
                        {!currentRoute && 
                            <div className="warning">Route deleted or never existed</div>
                        }
                        {currentRoute && 
                            <Fragment>
                                <EditSection user={props.user} adminPanel={props.adminPanel} data={props.data.routes} />
                                <IntroSection
                                    user={props.user}
                                    entity={"route"}
                                    entityId={currentRoute.id}
                                    title={`Route ${currentRoute.name}`} 
                                    desc={"Here you can find information about this route and check it out on the map."}
                                />
                                <DetailsSectionRouteCard 
                                    transportType={currentRoute.transportType}
                                    firstStop={currentRoute.firstStop}
                                    lastStop={currentRoute.lastStop}
                                    length={currentRoute.length}
                                    />
                                <StopsListSection 
                                    stopsNames={currentRoute.stopsNames}
                                    stopsId={currentRoute.stops}
                                />
                                <RouteCardMap route={currentRoute} stops={props.data.stops}/>
                            </Fragment>
                        }
                    </WrapperNarrowWhite>
                </WrapperWide>
                <SpacerWhite />
                <Footer />
            </PageWrapper>
        </Fragment>
    )
};



const mapStateToProps = store => {
    console.log(store);
    return {
        user: store.user,
        dropdowns: store.dropdowns,
        data: store.data,
        adminPanel: store.adminPanel
    };
}

export default connect(mapStateToProps)(RouteCard);