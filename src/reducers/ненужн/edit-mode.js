import { EDIT_MODE_ON } from 'actions/EditModeActions'
import { EDIT_MODE_OFF } from 'actions/EditModeActions'

export const initialState = {
    isEditMode: false //

}
  
export function editModeReducer(state = initialState, action) {
    switch (action.type) {
        case EDIT_MODE_ON:
            return { ...state, isEditMode: true }

        case EDIT_MODE_OFF:
            return { ...state, isEditMode: false }
    
        default:
            return state
    }
}