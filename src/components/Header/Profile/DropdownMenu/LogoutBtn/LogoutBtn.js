import React from 'react';
import {NavLink} from 'react-router-dom';
import { connect } from 'react-redux';
import {actions} from 'actions';

import './LogoutBtn.scss';

const LogoutBtn = ({onLogoutBtnClick}) => {
    return(
        <NavLink
            to="/auth">
            <button className="logout-btn profile__logout-btn" id="logout-btn"
                onClick={onLogoutBtnClick}>
                <i className="fas fa-sign-out-alt"></i>
            </button>
        </NavLink>
    )
};

// export {LogoutBtn};


const mapDispatchToProps = dispatch => {
    return {
        onLogoutBtnClick: () => dispatch(actions.UserActions.onLogoutBtnClick()),
        
    }
}


export default connect(
null,
mapDispatchToProps
)(LogoutBtn)

