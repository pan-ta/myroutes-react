import React, {Fragment} from 'react';
import './UpsertRoute.scss';
import {connect} from 'react-redux';
import {actions} from 'actions';


// const UpsertRoute = ({entityId, routes, addRoute, changeRoute}) => {

class UpsertRoute extends React.Component {
    constructor(props) { 
        super(props);
        // Don't call this.setState() here!
        // this.state = { counter: 0 };
        // this.handleClick = this.handleClick.bind(this);
        // this.handleClick = this.handleClick.bind(this);
        // this.handleClick = this.handleClick.bind(this);

        this.state = {
            stopInputsCounter: 8,

        }

        if (props.entityId) {
            props.routes.forEach(route => {
                if (route.id === this.props.entityId) {
                    this.state.thisRoute = route;
                }
            });

            this.state.stopInputsCounter = this.state.thisRoute.stops.length;
            this.state.title = `Edit route ${this.state.thisRoute.name}`;
            this.state.btnText = "Save";
        } else {
            this.state.title = "Add route";
            this.state.btnText = "Add new route";
        } 
    }

    onAddInputClick = (e) => {
        e.preventDefault();
        this.setState({
            stopInputsCounter : this.state.stopInputsCounter + 1
        });
    }

    render() {

         /////// add stop inputs
        const stopInputs = [];
        for (let i=0; i < this.state.stopInputsCounter; i++) {
            let inputName = `stopId${i + 1}`;
            stopInputs.push(
                <Fragment key={i}>
                    <div className="admin__block admin__block_narrow">
                        <input type="text" 
                        className="admin__input admin__input_narrow" 
                        name={inputName} 
                        placeholder="ID"
                        defaultValue = {this.props.entityId ? this.state.thisRoute.stops[i] : null}
                        />
                        <div className="admin__desc">Stop #{i+1}</div>
                    </div>
                </Fragment>
            )
        }

        const onSubmit = (e) => {

            e.preventDefault();
            if (this.props.entityId) {
              
                const stopsValues = [];
                for (let i = 1; i <= this.state.stopInputsCounter; i++) {
                    const stopValue = e.target["stopId" + i].value
                    if(stopValue) {
                       stopsValues.push(stopValue); 
                    }
                }

                this.props.upsertRoute("edit", e.target.routeId.value, e.target.routeName.value, e.target.transportType.value, stopsValues);
                this.props.closeOverlay();
            } else {
                 
                const stopsValues = [];
                for (let i = 1; i <= this.state.stopInputsCounter; i++) {
                    const stopValue = e.target["stopId" + i].value
                    if(stopValue) {
                       stopsValues.push(stopValue); 
                    }
                }

                this.props.upsertRoute("add", e.target.routeId.value, e.target.routeName.value, e.target.transportType.value, stopsValues);
                this.props.closeOverlay();
            }
        }
    
        return (
            <form className="admin__form" onSubmit={onSubmit}>
                <div className="admin__title">{this.state.title}</div>
                <div className="form-container">
                    <div className="admin__wrap">
                        <div className="admin__block admin__block_wide">
                            <input type="number" 
                                className="admin__input admin__input_wide" 
                                name="routeId" 
                                placeholder="route ID" 
                                defaultValue = {this.props.entityId ? this.state.thisRoute.id : null}
                            />
                            <div className="admin__desc">Route ID</div>
                        </div>
                        <div className="admin__block admin__block_wide">
                            <input type="text" 
                            className="admin__input admin__input_wide" 
                            name="routeName" placeholder="Route name" 
                            defaultValue = {this.props.entityId ? this.state.thisRoute.name : null}
                            />
                            <div className="admin__desc">Route name</div>
                        </div>
                        <div className="admin__block admin__block_wide">
                            <select name="transportType" 
                                className="admin__select" 
                                defaultValue = {this.props.entityId ? this.state.thisRoute.transportType : null}>
                                <option value="Bus">Bus</option>
                                <option value="Tram">Tram</option>
                                <option value="Taxi">Taxi</option>
                            </select>
                            <div className="admin__desc">Transport type</div>
                        </div>
                    </div>
                    <div className="admin__wrap" id="stops-block">
                        {stopInputs}
                        <div className="admin__block admin__block_narrow">
                            <button 
                                onClick={this.onAddInputClick} 
                                className="admin__add-input"
                            >
                                <i className="fas fa-plus-square"></i>
                                <span>Add stop</span>
                            </button>
                        </div>
                    </div>
                    
                </div>
                <button type="submit" className="submit-button button">{this.state.btnText}</button> 
            </form>
        );

        
    }
}


const mapDispatchToProps = dispatch => ({
    upsertRoute: (task, id, name, transportType, stops) => {
        dispatch(actions.DataActions.upsertRoute(task, id, name, transportType, stops));
    },
    closeOverlay: () => {
        dispatch(actions.AdminPanelActions.closeOverlay());
    }
})

export default connect(null, mapDispatchToProps)(UpsertRoute);


