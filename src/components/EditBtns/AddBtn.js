import React from 'react';
import './AddBtn.scss';




const AddBtn = ({text, onClick}) => (
    <button className="add-btn" onClick={onClick}>
        <i className="fas fa-plus-square"></i>
        <span>{text}</span>
    </button>
);


export default AddBtn;

