const initialData = {
    "stops": [
        {
            "id": 12345,
            "coord1": 59.951887, 
            "coord2": 30.237281,
            "name": "переулок Каховского",
            "address": "пер. Каховского, 3",
            "timeTable": [
                {
                    "routeName": "Автобус 40",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "12:30", "13:45", "14:00", "6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:30", "9:45", "10:00", "11:15", "12:30", "13:45", "14:00"]
                },
                {
                    "routeName": "Автобус 40R",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:30", "9:45", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:30", "9:45", "10:00", "11:15", "12:30", "13:45", "14:00"]
                }
            ]
            
        },
        {
            "id": 12346,
            "coord1": 59.951166, 
            "coord2": 30.246701,
            "name": "проспект КИМа",
            "address": "пр. КИМа, 9",
            "timeTable": [
                {
                    "routeName": "Автобус 40",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "12:30", "13:45", "14:00", "6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:30", "9:45", "10:00", "11:15", "12:30", "13:45", "14:00"]
                },
                {
                    "routeName": "Автобус 40R",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:30", "9:45", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:30", "9:45", "10:00", "11:15", "12:30", "13:45", "14:00"]
                }
            ]
        },
        {
            "id": 12347,
            "coord1": 59.947668, 
            "coord2": 30.255692,
            "name": "Смоленка",
            "address": "наб. реки Смоленки, 33",
            "timeTable": [
                {
                    "routeName": "Автобус 40",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "12:30", "13:45", "14:00", "6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:30", "9:45", "10:00", "11:15", "12:30", "13:45", "14:00"]
                },
                {
                    "routeName": "Автобус 40R",
                    "time": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:30", "9:45", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00", "8:30", "9:45", "10:00", "11:15", "12:30", "13:45", "14:00"]
                }
            ]
        }
    ],
    "routes": [
        {
            "id": 10040,
            "name": "40",
            "stops": [12347, 12346, 12345],
            "transportType": "Автобус",

        },
        {
            "id": 11040,
            "name": "40R",
            "stops": [12345, 12346, 12347],
            "transportType": "Автобус",

        }
    ],
    "transport": [
        {
            "id": 106789,
            "transportType": "Автобус",
            "model": "YUTONG ",
            "license": "MV345H",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106790,
            "transportType": "Автобус",
            "model": "YUTONG",
            "license": "JK874L",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106791,
            "transportType": "Автобус",
            "model": "Volvo FMX",
            "license": "II983Q",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106792,
            "transportType": "Автобус",
            "model": "Volvo FMX",
            "license": "PO981P",
            "routeId": 10040,
            "seats": 80
        },
        {
            "id": 116793,
            "transportType": "Маршрутное такси",
            "model": "Ford Transit",
            "license": "IIPOAQ",
            "routeId": 10040,
            "seats": 40
        },
        {
            "id": 106794,
            "transportType": "Автобус",
            "model": "YUTONG ",
            "license": "MV345H",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106795,
            "transportType": "Автобус",
            "model": "YUTONG",
            "license": "JK874L",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106796,
            "transportType": "Автобус",
            "model": "Volvo FMX",
            "license": "II983Q",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106797,
            "transportType": "Автобус",
            "model": "Volvo FMX",
            "license": "PO981P",
            "routeId": 10040,
            "seats": 80
        },
        {
            "id": 116798,
            "transportType": "Маршрутное такси",
            "model": "Ford Transit",
            "license": "IIPOAQ",
            "routeId": 10040,
            "seats": 40
        },
        {
            "id": 116799,
            "transportType": "Маршрутное такси",
            "model": "Ford Transit",
            "license": "IIPOAQ",
            "routeId": 10040,
            "seats": 40
        },
        {
            "id": 106750,
            "transportType": "Автобус",
            "model": "YUTONG ",
            "license": "MV345H",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106751,
            "transportType": "Автобус",
            "model": "YUTONG",
            "license": "JK874L",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106752,
            "transportType": "Автобус",
            "model": "Volvo FMX",
            "license": "II983Q",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106753,
            "transportType": "Автобус",
            "model": "Volvo FMX",
            "license": "PO981P",
            "routeId": 10040,
            "seats": 80
        },
        {
            "id": 116754,
            "transportType": "Маршрутное такси",
            "model": "Ford Transit",
            "license": "IIPOAQ",
            "routeId": 10040,
            "seats": 40
        },
        {
            "id": 106755,
            "transportType": "Автобус",
            "model": "Volvo FMX",
            "license": "PO981P",
            "routeId": 10040,
            "seats": 80
        },
        {
            "id": 116756,
            "transportType": "Маршрутное такси",
            "model": "Ford Transit",
            "license": "IIPOAQ",
            "routeId": 10040,
            "seats": 40
        },
        {
            "id": 116757,
            "transportType": "Маршрутное такси",
            "model": "Ford Transit",
            "license": "IIPOAQ",
            "routeId": 10040,
            "seats": 40
        },
        {
            "id": 106758,
            "transportType": "Автобус",
            "model": "YUTONG ",
            "license": "MV345H",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106759,
            "transportType": "Автобус",
            "model": "YUTONG",
            "license": "JK874L",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106760,
            "transportType": "Автобус",
            "model": "Volvo FMX",
            "license": "II983Q",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106761,
            "transportType": "Автобус",
            "model": "Volvo FMX",
            "license": "PO981P",
            "routeId": 10040,
            "seats": 80
        },
        {
            "id": 116762,
            "transportType": "Маршрутное такси",
            "model": "Ford Transit",
            "license": "IIPOAQ",
            "routeId": 10040,
            "seats": 40
        }
    ] 
}


const stopsData = [];
const routesData = [];
const transportData = [];

initialData.stops.forEach(stop => {
    let newStop = stop;
    newStop.routes = [];
    initialData.routes.forEach(route => {
        route.stops.forEach(routeStop => {
            if (routeStop === stop.id) {
                newStop.routes.push(`${route.transportType} ${route.name}`);
            }
        })
    })
    stopsData.push(newStop);

})

initialData.routes.forEach(route => {
    let newRoute = route;
    newRoute.fullName = `${route.transportType} ${route.name}`;
    newRoute.length = route.stops.length;
    newRoute.stopsNames = [];
    
    initialData.stops.forEach(stop => {
        newRoute.stopsNames.push(stop.name);
        if (stop.id === route.stops[0]) {
            newRoute.firstStop = stop.name;
        } else if (stop.id === route.stops[route.stops.length-1]) {
            newRoute.lastStop = stop.name;
        }
    });

    //crossings
    newRoute.crossRoutes = [];
    initialData.routes.forEach(neighbourRoute => {
        if (neighbourRoute.id !== route.id &&
            route.stops.some(stop => neighbourRoute.stops.includes(stop))) {
                newRoute.crossRoutes.push(neighbourRoute.name);
        }
    })
    
    routesData.push(newRoute);

})

initialData.transport.forEach(vehicle => {
    let newVehicle = vehicle;
    initialData.routes.forEach(route => {
        if (route.id === vehicle.routeId) {
            newVehicle.route = route.name;
        } 
    })

    transportData.push(newVehicle);

});



// console.log(RoutesData[0]);

export {transportData};
export {routesData}; 
export {stopsData};