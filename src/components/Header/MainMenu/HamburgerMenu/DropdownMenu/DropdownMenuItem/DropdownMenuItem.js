import React from 'react';
import {NavLink} from 'react-router-dom';
import './DropdownMenuItem.scss';


const DropdownMenuItem = ({link, text}) => {
    const thisLink = `/${link}`;
    return(
        <li className="dropdown__menu-item">
            <NavLink
                to={thisLink}>
                {text}
            </NavLink>
        </li>
    )
};

export {DropdownMenuItem};

