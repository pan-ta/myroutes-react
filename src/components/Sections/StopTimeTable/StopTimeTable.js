import React, {Fragment} from 'react';
// import {NavLink} from 'react-router-dom';
import './StopTimeTable.scss';


const StopTimeTable = ({timeTable}) => {
    const timeTableItems = [];
    timeTable.forEach((elem, elemI) => {
        const timeItems=[];
        elem.time.forEach((time, timeI) => {
            timeItems.push(
                <Fragment key={timeI}>
                    <li className="time-table__time-item">{time}</li>
                </Fragment>
            )
        });
        timeTableItems.push(
            <Fragment key={elemI}>
                <li className="time-table__item">
                    <div className="time-table__route">
                        {elem.routeName}
                    </div>
                    <ul className="time-table__time-list">
                        {timeItems}
                    </ul>
                </li>
            </Fragment>
        )
    });
    // const link = `/stop-card/${stopsId[i]}`;
    return (
        <section className="time-table section">
            <ul className="time-table__list">
                {timeTableItems}
            </ul>
        </section>
    )
};

export default StopTimeTable;

