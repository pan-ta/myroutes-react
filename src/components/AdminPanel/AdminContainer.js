import React from 'react';
import './AdminContainer.scss';




const AdminContainer = ({children}) => (
    <div className="admin__container">
        {children}
    </div>
);


export default AdminContainer;

