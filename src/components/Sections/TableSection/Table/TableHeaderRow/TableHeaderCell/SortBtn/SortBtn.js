import React from 'react';
import {connect} from 'react-redux';
import {actions} from 'actions';
import './SortBtn.scss';

const SortBtn = ({table, prop, currentSorting, changeCurrentSortingAction, sortDataAction, closeFilterBlocks, clearInputs}) => {

    const currentSortingValue = currentSorting[prop];
    let newValue = ((currentSortingValue === "asc") ? "desc" : "asc" );
    let isAsc = ((currentSortingValue === "asc") ? false : true );
    
    const onThisSortBtnClick = () => {
        changeCurrentSortingAction(table, prop, newValue);
        sortDataAction(table, prop, isAsc);
        closeFilterBlocks();
        clearInputs();
    };
    

    return (
        <button onClick={onThisSortBtnClick} className="sort-btn" >
            { currentSortingValue === null   && <i className="fas fa-sort"></i> }
            { currentSortingValue === "asc"  && <i className="fas fa-sort-up"></i> }
            { currentSortingValue === "desc" && <i className="fas fa-sort-down"></i> }
        </button>
    )
};


// export default SortBtn;

const mapDispatchToProps = dispatch => {
    return {
        changeCurrentSortingAction: (table, prop, newValue) => dispatch(actions.CurrentSortingActions.changeCurrentSorting(table, prop, newValue)),
        sortDataAction: (table, prop, isAsc) => dispatch(actions.DataActions.sortData(table, prop, isAsc)),
        closeFilterBlocks: () => dispatch(actions.TableFilterBlockVisibilityActions.closeFilterBlocks()),
        clearInputs: () => dispatch(actions.TableFilterInputActions.clearInputs())
    }
}
  
export default connect(
    null,
    mapDispatchToProps
)(SortBtn)

