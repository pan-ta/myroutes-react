import React from 'react';


class YMap extends React.Component {

    componentDidMount() {
        window.ymaps.ready(this.init.bind(this));
    }

    // getReferencePoints = () => {
    //     const allStops = this.props.stops;
    //     return this.props.route.stops.map(routeStopId => {
    //         const routeStop = allStops.find(stop => stop.id === routeStopId);
    //         return [
    //             routeStop.coord1,
    //             routeStop.coord2
    //         ]
    //     });
    // }

    init = () => {
        /**
         * Создаем мультимаршрут.
         * Первым аргументом передаем модель либо объект описания модели.
         * Вторым аргументом передаем опции отображения мультимаршрута.
         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRoute.xml
         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRouteModel.xml
         */
        var multiRoute = new window.ymaps.multiRouter.MultiRoute({
            // Описание опорных точек мультимаршрута.
            // referencePoints: this.getReferencePoints(),
            referencePoints: [
                [51.216695, 3.197463], 
                [51.221057, 3.218440],
                [51.222809, 3.232073],
                [51.209684, 3.232201],
                [51.201839, 3.239818], 
                [51.201947, 3.255740]],
            // Параметры маршрутизации.
            params: {
                // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                results: 1
            }
        }, {
            // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
            boundsAutoApply: true,
            wayPointIconColor: "#fff",
            wayPointIconFillColor: "#CD7233", 
            wayPointStartIconColor: "#fff",
            wayPointStartIconFillColor: "#CD7233", 
            wayPointFinishIconColor: "#fff",
            wayPointFinishIconFillColor: "#CD7233", 

            

        });
    
        // Создаем карту с добавленными на нее кнопками.
        var myMap = new window.ymaps.Map('map', {
            center: [51.193366, 3.197488],
            zoom: 12,
            controls: ['smallMapDefaultSet']
        }, {
    
        });
    
        // Добавляем мультимаршрут на карту.
       myMap.geoObjects.add(multiRoute);
    }

    render() {
        return <div id="map" style={{ "height" : "90vh" , "width" : "100vw"}}></div>
    }
}

export default YMap;