import React from 'react';
import './TableCheckbox.scss';



const TableCheckbox = () => {
    return(
        <div className="filter-block__checkboxes" hidden>
            <label for="transport-type" className="filter-block__checkbox-item">
                <input type="checkbox" className="checkbox" name="transport-type" value="Bus" checked></input>
                <span className="checkbox-custom"></span>
                <span className="label">Автобус</span>
            </label>
            <label for="transport-type" className="filter-block__checkbox-item">
                <input type="checkbox" className="checkbox" name="transport-type" value="taxi" checked></input>
                <span className="checkbox-custom"></span>
                <span className="label">Маршрутное такси</span>
            </label>
        </div>
    )
};

export {TableCheckbox};

