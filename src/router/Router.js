import React from 'react';
import { Route, Switch } from 'react-router-dom';

import HomePage from './Pages/HomePage';
import AuthPage from './Pages/AuthPage.js';
import TransportPage from './Pages/TransportPage.js';
import RoutesPage from './Pages/RoutesPage.js';
import StopsPage from './Pages/StopsPage.js';
import MapPage from './Pages/MapPage.js';
import RouteCard from './Pages/RouteCard.js';
import StopCard from './Pages/StopCard.js';
import VehicleCard from './Pages/VehicleCard.js';

const Router = () => (
    <Switch>
        <Route
            exact
            path="/"
            component={HomePage}
        />
        <Route
            path="/auth"
            component={AuthPage}
        />
        <Route
            path="/transport"
            component={TransportPage}
        />
        <Route
            path="/routes"
            component={RoutesPage}
        />
        <Route
            path="/stops"
            component={StopsPage}
        />
        <Route
            path="/map"
            component={MapPage}
        />
        <Route
            path="/vehicle-card/:vehicleId"
            component={VehicleCard}
        />
        <Route
            path="/route-card/:routeId"
            component={RouteCard}
        />
        <Route
            path="/stop-card/:stopId"
            component={StopCard}
        />
    </Switch> 
);

export default Router;