import React from 'react';
import './BigMap.scss';
import Map from './Map';


const BigMap = ({route, stops}) => {

    return (
        <div className="big-map">
            <Map route={route} stops={stops}/>
        </div>
    )
};

export default BigMap;

