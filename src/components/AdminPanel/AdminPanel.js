import React from 'react';
import './AdminPanel.scss';

import AdminCloseBtn from './components/AdminCloseBtn'

import DeleteEntity from './DeleteEntity/DeleteEntity'
import UpsertRoute from './UpsertRoute/UpsertRoute'
import UpsertStop from './UpsertStop/UpsertStop'
import UpsertVehicle from './UpsertVehicle/UpsertVehicle'
import UpsertNews from './UpsertNews/UpsertNews'


const AdminPanel = ({adminPanel, onCloseClick, data}) => {
    let content;

    if (adminPanel.task !== "delete") {
        switch (adminPanel.entity) {
            case ("route"):
                content = <UpsertRoute entityId={adminPanel.entityId} routes={data} />;
                break;
            case ("stop"):
                content = <UpsertStop entityId={adminPanel.entityId} stops={data} />;
                break;
            case ("vehicle"):
                content = <UpsertVehicle entityId={adminPanel.entityId} vehicles={data} />;
                break;
            case ("news"):
                content = <UpsertNews />;
                break;
            default:
                content = "wrong task"
        }
    } else {
        content = <DeleteEntity entity={adminPanel.entity} entityId={adminPanel.entityId} />;
    }

    return (
        <div className="admin-panel">
            <div className="admin-panel__overlay overlay">
                <div className="admin-panel__popup">
                    <AdminCloseBtn id="close-btn_admin" onClick={onCloseClick}/>
                    <div className="admin-panel__content">
                        {content}
                    </div>
                </div>
            </div>
        </div>
    )
};


export default AdminPanel;

