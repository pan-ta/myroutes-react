import React from 'react';
import './MainMenu.scss';

import {StringMenu} from './StringMenu/StringMenu';
import {HamburgerMenu} from './HamburgerMenu/HamburgerMenu';


const MainMenu = ({dropdowns}) => {
    return(
        <nav className="main-menu">
            <StringMenu />
            <HamburgerMenu dropdowns={dropdowns} />
        </nav>
    )
};

export {MainMenu};

