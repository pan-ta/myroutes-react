import React from 'react';
import "./WrapperWithMap.scss";

const WrapperWithMap = ({children}) => (
    <div className="wrapper-witn-map" >
        {children}
    </div>
);

export default WrapperWithMap;