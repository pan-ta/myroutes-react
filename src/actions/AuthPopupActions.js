
export const OPEN_LOGIN = 'OPEN_LOGIN';
export const OPEN_RESTORE = 'OPEN_RESTORE';
export const OPEN_REGISTER = 'OPEN_REGISTER';
export const SKIP_REGISTRATION = 'SKIP_REGISTRATION';

export function onLoginBtnClick() {
    return {
        type: OPEN_LOGIN,
        payload: null
    }
}

export function onRestorePassBtnClick() {
    return {
        type: OPEN_RESTORE,
        payload: null
    }
}

export function onRegisterBtnClick() {
    return {
        type: OPEN_REGISTER,
        payload: null
    }
}

export function onSkipRegistrationBtnClick() {
    return {
        type: SKIP_REGISTRATION,
        payload: null
    }
}
