import React from 'react';
import './SocialsItem.scss';


const SocialsItem = ({link, icon}) =>
    (
        <a href={link} className="socials__link">
            <i className={icon}></i>
        </a>
    );

export {SocialsItem};

