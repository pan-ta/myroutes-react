import React, {Fragment} from 'react';

import {connect} from 'react-redux';
import {actions} from 'actions';

import './EditSection.scss';
import AdminPanel from 'components/AdminPanel/AdminPanel'

const EditSection = ({user, adminPanel, onAddClick, onCloseClick, data}) => {
    return (
        <Fragment>
            {user.isAdmin &&
                <section className="edit section">
                    <div className="edit__top-buttons-wrapper">
                        <button className="edit__top-button" onClick={onAddClick("add", "vehicle", null)}>Add Vehicle</button>
                        <button className="edit__top-button" onClick={onAddClick("add", "route", null)}>Add Route</button>
                        <button className="edit__top-button" onClick={onAddClick("add", "stop", null)}>Add Stop</button>
                        <button className="edit__top-button" 
                            // onClick={onAddClick("add", "news", null)}
                            >Add News
                        </button>
                    </div>
                    {adminPanel.isOverlayOpen &&
                        <AdminPanel adminPanel={adminPanel} data={data} onCloseClick={onCloseClick} />
                    }
                </section>
            }
     </Fragment>       
)};




const mapDispatchToProps = dispatch => ({
    onAddClick: (task, entity, entityId) => () => {
        dispatch(actions.AdminPanelActions.openOverlay(task, entity, entityId));
    },
    onCloseClick: () => {
        dispatch(actions.AdminPanelActions.closeOverlay());
    }
})

export default connect(null, mapDispatchToProps)(EditSection);

