import {routesData, stopsData, transportData} from 'data/dataBruges';

import {
    SORT_DATA,
    FILTER_DATA,
    UPSERT_ROUTE,
    UPSERT_STOP,
    UPSERT_VEHICLE,
    DELETE_ENTITY
} from 'actions/DataActions';

export const initialState = {
    routes: routesData, 
    stops: stopsData,
    transport: transportData
}
  
export function dataReducer(state = initialState, action) {
    switch (action.type) {
        case SORT_DATA:
            console.log("сортируем данные по параметрам");
            let sortedData = sortDataByProp(state[action.payload.table].slice(0), action.payload.prop, action.payload.isAsc)
            let newSortState = {};
            newSortState[action.payload.table] = sortedData;
            return {...state, ...newSortState};

        case FILTER_DATA:
            console.log("фильтруем данные по параметрам");
            if (action.payload.inputValue) {
                let filteredData = filterDataByPropAndInputValue(initialState[action.payload.table].slice(0), action.payload.prop, action.payload.inputValue)
                let newFilterState = {};
                newFilterState[action.payload.table] = filteredData;
                return {...state, ...newFilterState};
            } else {
                return initialState
            }

        case DELETE_ENTITY:
            console.log("удаляем сущность");
            let entityArr=[];
            if (action.payload.entity === "route") {
                entityArr = initialState.routes;
            } else if (action.payload.entity === "stop") {
                entityArr = initialState.stops;
            } else if (action.payload.entity === "vehicle") {
                entityArr = initialState.transport;
            }

            const entityId = action.payload.id; 
            console.log(entityArr, entityId);
            entityArr.forEach(entity => {
                if (entity.id === +entityId) {
                    // entityArr.delete(entity);
                    let i = entityArr.indexOf(entity);
                    entityArr.splice(i, 1);
                }
            })
            return initialState;


        // case ADD_STOP:
        //     console.log("добавляем остановку");



            
        case UPSERT_ROUTE:

            if (action.payload.task === "edit") {
                initialState.routes.forEach(route => {
                    if (route.id === +action.payload.id) {
                        // entityArr.delete(entity);
                        let i = initialState.routes.indexOf(route);
                        initialState.routes.splice(i, 1);
                    }
                })
            }

            const newRoute = {
                id: +action.payload.id,
                name: action.payload.name,
                transportType: action.payload.transportType,
                stops: action.payload.stops.map(stopId => parseInt(stopId)),
            };

            console.log("добавляем маршрут" , newRoute);

            newRoute.fullName = `${action.payload.transportType} ${action.payload.name}`;
            newRoute.length = action.payload.stops.length;
            newRoute.stopsNames = [];

            const stopsLookupIdName = {};
            initialState.stops.forEach(stop => stopsLookupIdName[stop.id] = stop.name);

            newRoute.stopsNames = action.payload.stops.map(stopId => stopsLookupIdName[stopId]);
            newRoute.firstStop = stopsLookupIdName[action.payload.stops[0]];
            newRoute.lastStop = stopsLookupIdName[action.payload.stops[action.payload.stops.length - 1]];
            
            newRoute.crossRoutes = [];
            initialState.routes.forEach(neighbourRoute => {
                if (neighbourRoute.id !== +action.payload.id &&
                    action.payload.stops.some(stop => neighbourRoute.stops.includes(stop))) {
                        newRoute.crossRoutes.push(neighbourRoute.name);
                }
            })

            initialState.routes.push(newRoute);
            return initialState

        case UPSERT_STOP:

            if (action.payload.task === "edit") {
                initialState.stops.forEach(stop => {
                    if (stop.id === +action.payload.id) {
                        let i = initialState.stops.indexOf(stop);
                        initialState.stops.splice(i, 1);
                    }
                })
            }

            const newStop = {
                id: +action.payload.id,
                name: action.payload.name,
                address: action.payload.address,
                coord1: action.payload.coord1,
                coord2: action.payload.coord2,
            };

            newStop.routes = [];
            initialState.routes.forEach(route => {
                route.stops.forEach(routeStop => {
                    if (routeStop === +action.payload.id) { 
                        newStop.routes.push(`${route.transportType} ${route.name}`);
                    }
                })
            })

            initialState.stops.push(newStop);
            return initialState


        case UPSERT_VEHICLE:

            if (action.payload.task === "edit") {
                initialState.transport.forEach(vehicle => {
                    if (vehicle.id === +action.payload.id) {
                        let i = initialState.transport.indexOf(vehicle);
                        initialState.transport.splice(i, 1);
                    }
                })
            }

            const newVehicle = {
                id: +action.payload.id,
                license: action.payload.license,
                model: action.payload.model,
                transportType: action.payload.transportType,
                seats: action.payload.seats,
                route: action.payload.route,
            };


            initialState.transport.push(newVehicle);
            return initialState



        default:
            return state
    }
}


//////////////////




function sortDataByProp(data, prop, isAsc) {

    function comparePropsAsc(a, b) {
        if (a[prop] > b[prop]) return 1;
        if (a[prop] < b[prop]) return -1;
    }
    function comparePropsDesc(a, b) {
        if (a[prop] > b[prop]) return -1;
        if (a[prop] < b[prop]) return 1;
    }
    let sortedData;

    if (isAsc) {
        sortedData = data.sort(comparePropsAsc);
    } else {
        sortedData = data.sort(comparePropsDesc);
    }

    return sortedData;

}

function filterDataByPropAndInputValue(data, prop, inputValue) {
    
    let filteredData = [];
    data.forEach(elem => {
        if(elem[prop].toString().toUpperCase().includes(inputValue.toUpperCase())) {
            filteredData.push(elem);
        }
    })
    return filteredData
}



// function getAutocompleteValues(inputValue, allValues) {
//     const autocompleteValues = [];
//     allValues.forEach(value => {
//         if (value.toString().substr(0, inputValue.length).toUpperCase() === inputValue.toUpperCase()) {
//             autocompleteValues.push(value);
//         }
//     });
//     return autocompleteValues;
// }


