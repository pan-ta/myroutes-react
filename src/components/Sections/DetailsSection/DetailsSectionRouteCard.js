import React from 'react';
import './DetailsSection.scss';



const DetailsSectionRouteCard = ({transportType, firstStop, lastStop, length, crossRoutes}) => {
    return(
        <div className="details-section section">
            <div className="details-section__string">
                <span>Transport type: </span>
                <span>{transportType}</span>
            </div>
            <div className="details-section__string">
                <span>First stop: </span>
                <span>{firstStop}</span>
            </div>
            <div className="details-section__string">
                <span>Last stop: </span>
                <span>{lastStop}</span>
            </div>
            <div className="details-section__string">
                <span>Number of stops: </span>
                <span>{length}</span>
            </div>
            {/* <div className="details-section__string">
                <span>Пересекается с маршрутами: </span>
                <span>{crossRoutes}</span>
            </div> */}
        </div>
    )
};

export {DetailsSectionRouteCard};

