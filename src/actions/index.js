import * as UserActions from './UserActions.js' 
import * as DropdownActions from './DropdownsActions.js' 
import * as AuthPopupActions from './AuthPopupActions.js' 
import * as DataActions from './DataActions.js' 
import * as CurrentSortingActions from './CurrentSortingActions.js'
import * as TableFilterBlockVisibilityActions from './TableFilterBlockVisibilityActions.js' 
import * as TableFilterInputActions from './TableFilterInputActions.js'
import * as AdminPanelActions from './AdminPanelActions.js'


const actions = {
    UserActions,
    DropdownActions,
    AuthPopupActions,
    DataActions,
    CurrentSortingActions,
    TableFilterBlockVisibilityActions,
    TableFilterInputActions,
    AdminPanelActions
    
};

export {actions}; 