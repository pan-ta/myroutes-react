import React from 'react';
import './ProfileBtn.scss';


const ProfileBtn = ({onClick}) => (
    <button className="profile__btn" id="profile-btn" onClick={onClick}>
        <i className="far fa-user-circle"></i>
    </button>
)

export {ProfileBtn};

