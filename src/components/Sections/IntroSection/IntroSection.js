import React, {Fragment} from 'react';
import './IntroSection.scss';

import {connect} from 'react-redux';
import {actions} from 'actions';

import SmallEditBtn from 'components/EditBtns/SmallEditBtn';
import SmallDeleteBtn from 'components/EditBtns/SmallDeleteBtn';

const IntroSection = ({user, title, desc, entity, entityId, onEditClick, onDeleteClick}) => (
    <section className="intro section">
    
        <h1 className="intro__title title">{title}</h1>
        {user.isAdmin && entity &&
            <div className="edit-card-btns">
                <Fragment>
                    <SmallEditBtn onClick={onEditClick(entity, +entityId)}/>
                    <SmallDeleteBtn onClick={onDeleteClick(entity, +entityId)}/>
                </Fragment>
            </div>
        }
        <div className="intro__desc description">
            <span>
                {desc}
            </span>
        </div>
        
    </section>
);

//export default IntroSection;

const mapDispatchToProps = dispatch => ({
    onEditClick: (entity, entityId) => () => {
        dispatch(actions.AdminPanelActions.openOverlay("edit", entity, entityId));
    },
    onDeleteClick: (entity, entityId) => () => {
        dispatch(actions.AdminPanelActions.openOverlay("delete", entity, entityId));
    }
})

export default connect(null, mapDispatchToProps)(IntroSection);

