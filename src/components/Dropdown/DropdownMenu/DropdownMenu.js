import React from 'react';
import './DropdownMenu.scss';

import {DropdownMenuItem} from './DropdownMenuItem/DropdownMenuItem.js';

const DropdownMenu = ({dropdowns}) => {
    return(
        <div className="dropdown hamburger__dropdown" id="hamburger-dropdown" 
            hidden={dropdowns.isVisibleHamburgerDropdown ? false : true}>
            <div className="dropdown__arrow"></div>
            <ul className="dropdown__menu-list">
                <DropdownMenuItem link={"transport"} text={"Vehicles"} />
                <DropdownMenuItem link={"routes"} text={"Routes"} />
                <DropdownMenuItem link={"stops"} text={"Stops"} />
                <DropdownMenuItem link={"map"} text={"Find route"} />
            </ul>
            <div className="dropdown__extras">

            </div>
        </div>
    )
};

export {DropdownMenu};

